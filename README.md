# The citation link adapter #

This is one of the "data adapters" for the Scholarly Artifacts Knowledge Graph (KG) project.
It harvests, parses, enriches, converts to RDF and stores citation links to the KG.

`LinkHarvester` interface: As each collection provides certain access mechanism
(HTML, API, etc.), this is the interface one extends to consider (and implement) the
specific details of adding a citation link collection resources to the KG.


### What is this repository for? ###

* This repo contains the implementation of a workflow to convert ScholeXplorer link collection (JSON data dump) to RDF.
* Version: 1.0.0

### How do I get set up? ###

- The input link collection (ScholeXplorer, in this case) needs to be copied to *resources/input* directory. The 
application (1) splits the files stored in resources/input directory into smaller batches of files, and stores them in 
*resources/processed* directory. Once this completes, (2) the RDF conversion starts; the results (RDF triples) are stored 
the results in the *resources/output* - both in an Apache Jena TDB dataset and a RDF data dump, in an N-Quads format and 
compressed.

+ Project depndencies include
    + Java 11
    + Maven, and
    + Apache Jena Framework: Used for RDF conversion and storage, thus it must be setup (put on the path) on your system.

### Running the application ###  

1. **Programmatically**
The main class of the application is *app.MainApp*. The *runApp()* method does both splitting and converting the files 
to RDF. You can choose to first complete the process of splitting into smaller files, and then (re run) the app (keep 
only the method *fileIO.splitFilesFromDir(...)*) and then conduct only the RDF conversion (comment out the method that does the splitting of files).

2. **Via Command-line instructions**
The prerequisites include the following components:

```
Clone the [project](https://bitbucket.org/fidanLimani/scholexplorer-rdf-conversion/src/master/)
cd to project
# Compile and run
mvn mvn clean package

# Run the executable jar; we confiured an 80 GB heap space for the JVM, you can change it to whatever is available to 
# your environment.
java -Xmx80G -jar target/scholexplorer-rdf-conversion-1.0-SNAPSHOT.jar
```