FROM azul/zulu-openjdk-alpine:16

# Maven
RUN apk add maven=3.6.3-r0

# Install bash in order to explore the contents of the container
RUN apk add bash

# Environmental variable for Apache Jena version
ENV JENA_VERSION 4.0.0
RUN mkdir -p /apache-jena/apache-jena-$JENA_VERSION && \
    wget https://mirror.netcologne.de/apache.org/jena/binaries/apache-jena-$JENA_VERSION.tar.gz -O - | tar -xz -C /apache-jena && \
    mv apache-jena/apache-jena-$JENA_VERSION/* /apache-jena && \
    rm -r apache-jena/apache-jena-$JENA_VERSION && \
    cd /apache-jena && \
    rm -rf *javadoc* *src* bat

# Check Apache Jena installation
# RUN riot  --version

# Environmental variables for Apache Fuseki
ENV FUSEKI_VERSION 4.0.0
ENV FUSEKI_BASE /fuseki

# Installation folder
ENV FUSEKI_HOME /apache-jena-fuseki

# RUN java -XX:+PrintFlagsFinal -version | grep HeapSize

# WORKDIR /tmp
RUN mkdir -p /$FUSEKI_HOME/$FUSEKI_HOME-$FUSEKI_VERSION && \
    wget https://mirror.netcologne.de/apache.org/jena/binaries/apache-jena-fuseki-$FUSEKI_VERSION.tar.gz -O - | tar -xz -C /$FUSEKI_HOME && \
    mv $FUSEKI_HOME/$FUSEKI_HOME-$FUSEKI_VERSION/* /$FUSEKI_HOME && \
    rm -r $FUSEKI_HOME/$FUSEKI_HOME-$FUSEKI_VERSION && \
    cd /$FUSEKI_HOME && \
    rm -rf fuseki.war && chmod 755 fuseki-server

# Include the Java appliation
COPY src /zbw/lim/semantic-links/src
COPY pom.xml /zbw/lim/semantic-links/pom.xml

# Change current directory to the Java project
WORKDIR /zbw/lmn/semantic-links

# Run Maven package (during image building)
RUN mvn clean package

# If we want a specific heap size, we can use the appropriate flag:
CMD ["java", "-Xmx30G", "-jar", "target/scholexplorer-rdf-conversion-1.0-SNAPSHOT.jar"]