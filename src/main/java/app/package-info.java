/*
    This package brings together all the classes to:
    - Harvest (create) links
        (1) Establish links between JDA-to-EconBiz resources
        (2) Harvest links from ScholExplorer

    - Convert links to RDF and store them
        (3) Convert JDA-to-EconBiz links in RDF and store them in a named graph
        (4) Convert ScholExplore links in RDF and store them in a named graph

    - Use case application
        (5) SPARQL queries to the combined graph of both link types.
**/
package app;