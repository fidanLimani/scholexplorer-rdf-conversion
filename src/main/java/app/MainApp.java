package app;

import harvest.ScholExploreHarvester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.FileIO;

import java.nio.file.Path;

import static constants.HarvestLogging.*;

/**
 * This class brings together the classes and method calls required to parse, convert, and store the ScholExplore
 * link collection to RDF.
 **/
public class MainApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainApp.class);

    // This field is required to pre-process the original Scholexplorer collection into smaller files
    private FileIO fileIO;

    // Parser and harvest the link collection (in JSON)
    private ScholExploreHarvester scholExploreHarvester;

    // No-args constructor
    public MainApp () {
        scholExploreHarvester = new ScholExploreHarvester();
        fileIO = new FileIO();
    }

    /**
     * The entry method for the whole application
     */
    public void runApp() {
        LOGGER.info("Harvesting, parsing and conversion of ScholeXplorer link collection has started...");
        scholExploreHarvester.harvestLinkCollectionFromDir(Path.of(SCHOL_EXPLORER_INPUT));

        // With RDF conversion and storage complete, SPARQL query and store the results to a file (Optional)
        // scholExploreHarvester.getRdfStore().generateDatasetStats(Path.of(String.format(SCHOL_EXPLORER_OUTPUT, SPARQL_OUT_FILE)).toFile());
    }

    // Run the app
    public static void main(String[] args){
        MainApp mainApp = new MainApp();
        mainApp.runApp();
        System.out.println(); // Used to make logs easier to read
        LOGGER.info("Application completed successfully.");
    }
}