package utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.Year;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.regex.Pattern;

public class StringUtils {
    private static final Pattern DATE_PATTERN1 = Pattern.compile("^[A-Z][a-z]+ \\d{1,2}, \\d{4}");
    private static final Pattern DATE_PATTERN2 = Pattern.compile("^\\d{1,2} [A-Z][a-z]+ \\d{4}");

    public static String filterDate(String inputDate) {
        String fliteredInputDate;

        // Remove trailing space and the weird "‐" character that differs from the normal dash ("-") for some reason :(
        fliteredInputDate = inputDate.trim().replace("‐", "-").replace("--", "-");

        return fliteredInputDate;
    }

    /**
     * Assess the DateTimeFormatter for a date value
     * @param inputDate The date value for which we need to assess the DateTimeFormatter
     * @return DateTimeFormatter The format that matches the date value
     */
    public static DateTimeFormatter assessDateFormatter(String inputDate) {
        // Input example: 2019-05-06 (alternative with '/' should also be considered)
        DateTimeFormatter inputDateFormatter;
        String[] dateParts = new String[3];

        if(inputDate.contains("-")){
            dateParts = inputDate.split("-");
        }

        if(inputDate.contains("/")){
            dateParts = inputDate.split("/");
        }

        /* Check in the following order:
            0. Year-month-day: Does it start with a year?
            1. If it does not start with a year, parse it as "Month-day-year" (US-based) until another issue (from)
            "Day-month-year" patterns emerges.
        */
        if (dateParts[0].length() == 4) {
            // Try to parse it as "Year-Month-Day" (ISO 8601) or "Year-Day-Month" pattern
            if (Integer.parseInt(dateParts[1]) <= 12){
                inputDateFormatter = DateTimeFormatter.ofPattern("y-MM-dd");
            } else {
                inputDateFormatter = DateTimeFormatter.ofPattern("y-dd-MM");
            }
        } else {
            // Parse it as Day-Month-Year;
            inputDateFormatter = DateTimeFormatter.ofPattern("dd-MM-y");
        }

        return inputDateFormatter;
    }

    public static LocalDate handleDateFormat(String inputDate) {
        LocalDate theDate = LocalDate.now();
        DateTimeFormatter formatter;

        String filteredInputDate = filterDate(inputDate);
        int inputLength = filteredInputDate.length();

        // Are date values separated by space?
        if (filteredInputDate.contains(" ")) {
            if (inputLength == 19) { // 2009-12-24 19:02:17
                formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else if (DATE_PATTERN1.matcher(filteredInputDate).matches()) { // "January 9, 2018" pattern
                formatter = DateTimeFormatter.ofPattern("MMMM d, y", Locale.ENGLISH);
                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else if(DATE_PATTERN2.matcher(filteredInputDate).matches()) { // 20 October 2016
                formatter = DateTimeFormatter.ofPattern("dd MMMM y", Locale.ENGLISH);
                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else {
                // Case: "December 2015"
                formatter = DateTimeFormatter.ofPattern("MMMM y", Locale.ENGLISH);
                YearMonth yearMonth = YearMonth.parse(filteredInputDate, formatter);
                theDate = yearMonth.atDay(1);
            }
        } else if (filteredInputDate.contains(".")) {
            if(inputLength == 10) { // 19.02.2014
                formatter = DateTimeFormatter.ofPattern("dd.MM.y");
                theDate = LocalDate.parse(filteredInputDate, formatter);
            }
        } else { // '-' or other characters are used to construct the date
            if (inputLength == 4) { // E.g., "2019"
                theDate = Year.parse(filteredInputDate).atDay(1);
            } else if (inputLength == 6) { // 2018-0
                theDate = Year.parse(filteredInputDate.substring(0, filteredInputDate.indexOf("-"))).atDay(1);
            } else if (inputLength == 7) { // E.g., "2013-04"
                theDate = YearMonth.parse(filteredInputDate).atDay(1);
            } else if(inputLength == 8) { // E.g., 06-03-15 -> June 3 2017
                String[] dateParts = filteredInputDate.split("-");

                if(Integer.parseInt(dateParts[0]) <= 12) {
                    // If the first part of the date value > 12
                    formatter = DateTimeFormatter.ofPattern("MM-dd-yy");
                } else {
                    formatter = DateTimeFormatter.ofPattern("dd-MM-yy");
                }

                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else if(inputLength == 9) { // E.g., 5-08-2017 -> May 8 2017
                formatter = DateTimeFormatter.ofPattern("d-MM-y");
                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else if(inputLength == 10) { // 2 cases: Day-Month-Year (28-12-2018), and Year-Day-Month (2016-13-04)
                formatter = assessDateFormatter(filteredInputDate);
                theDate = LocalDate.parse(filteredInputDate, formatter);
            } else if (inputLength == 20) { // The date is in a ZonedDateTime format
                theDate = ZonedDateTime.parse(filteredInputDate).toLocalDate();
            } else if (inputLength == 21) { // 2018-11-27/2018-11-28
                String[] dateParts = filteredInputDate.split("/");
                formatter = DateTimeFormatter.ofPattern("y-MM-dd");
                // Consider the first date value from out of the two available:
                theDate = LocalDate.parse(dateParts[0], formatter);
            } else if (inputLength == 24) { // 2018-12-12T18:04:05.754Z
                formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                theDate = LocalDate.parse(filteredInputDate, formatter);
            }
            else if (inputLength == 32) { // 2018-06-26T12:41:47.396048+00:00
                formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX");
                theDate = LocalDate.parse(filteredInputDate, formatter);
            }
            else { // The date is in a LocalDate format
                theDate = LocalDate.parse(filteredInputDate);
            }
        }

        return theDate;
    }

    /**
     * Given a LocalDateTime instance, format (and return) the date and time only
     * @param dateAndTime The dateAndTime to be converted
     * @return formatedDateTime A string value that contains the date and time of a LocalDateTime instance
     */
    public static String formatDateAndTime(LocalDateTime dateAndTime){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateAndTime.format(dateTimeFormatter);
    }
}