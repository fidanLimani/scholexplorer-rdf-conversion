package utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Stream;

import static constants.HarvestLogging.*;

/* This class has a method that creates a file with the name of the dataset ID, and stores its content */
public class FileIO {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileIO.class);
    private ProgressBarBuilder progressBarBuilder;

    public FileIO(){
        // Setup the ProgressBars
        progressBarBuilder = new ProgressBarBuilder()
                .setTaskName("Reading")
                .setUnit("KB", 2048)
                .setStyle(ProgressBarStyle.COLORFUL_UNICODE_BLOCK);
    }

    /**
     * Append string to file contents
     */
    public static void appendToFile(String data, File file) throws IOException {
        FileWriter fr = new FileWriter(file, true);
        BufferedWriter br = new BufferedWriter(fr);
        br.write(data + "\n");

        br.close();
        fr.close();
    }

    /**
     * Delete a directory (and all its sub-directories), used to create free space from a Apache Jena TDB instance.
     * @param targetDirectory The directory (category) to delete: Directories from TDB (contain "Data-" in their name)
     **/
    public static void deleteDirectories(String targetDirectory) {
        // Traverse the file tree in depth-first fashion and delete each file/directory.
        try (Stream<Path> walk = Files.walk(Paths.get(targetDirectory))) {
            walk.sorted(Comparator.reverseOrder())
                    .filter(s -> s.toString().contains("Data-"))
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Split the contents of each ScholExplorer file based on a certain # of links and store it over several, smaller ones
     */
    public void splitFile(File file) throws IOException {
        LOGGER.info("Splitting the file: " + file.getName());

        // Read individual JSON entries from the file
        try (BufferedReader br = new BufferedReader(new InputStreamReader(ProgressBar.wrap(
                new FileInputStream(file.getPath()), progressBarBuilder)))) {
            String tempData;
            int batchLinkCounter = 0;
            int batchNumber = 0;
            String batchFileNamePattern = "%s-%d";

            // Create batches of List<String> of Link instances to store on a new, smaller file
            List<String> linkBatch = new ArrayList<>();

            // Loop through the file contents
            while ((tempData = br.readLine()) != null) {
                batchLinkCounter++;

                linkBatch.add(tempData);

                // If the linkBatch has reached <linkCounter> entries, store it to a new file
                if(batchLinkCounter % LINK_FILE_BATCH == 0){
                    // Create the file name for the batch
                    String batchFileName = String.format(batchFileNamePattern, file.getName(), batchNumber);
                    // System.out.println(batchFileName);

                    // Create a new file with its name reflecting the batch # & Store the contents of <linkBatch> to it
                    // Store the batch of links
                    storeLinkBatchToFile(linkBatch, Path.of(String.format(SCHOL_EXPLORER_PROCESSED, batchFileName))
                            .toFile());
                    linkBatch.clear();
                    batchLinkCounter = 0;
                    batchNumber++;
                }
            }

            // In this part add all the remaining Link instances that did not "make" a batch
            System.out.printf("Storing the %d remaining items after the last batch: \n", batchLinkCounter);
            // Create the file name for the batch
            String batchFileName = String.format(batchFileNamePattern, file.getName(), batchNumber);
            storeLinkBatchToFile(linkBatch, Path.of(String.format(SCHOL_EXPLORER_PROCESSED, batchFileName))
                    .toFile());

            // Update the total # of batches created for the original file
            batchNumber++;

            System.out.printf("A total of %d batches created\n", batchNumber);
        }

        // # of links parsed from a file
        // System.out.println("# of links parsed: " + numberOfLinks);

        // Introduce a pause in-between file parsing
        try {
            TimeUnit.MILLISECONDS.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Given a List<String> and a file name: (1) Create the file, and (2) store/append the list to it
     * @param stringList The contents to append
     * @param file The file we are appending the contents to
     */
    public void storeLinkBatchToFile(List<String> stringList, File file) throws IOException {
        FileWriter fr = new FileWriter(file, true);
        BufferedWriter br = new BufferedWriter(fr);

        for(String link : stringList) {
            br.write(link);
            br.newLine();
        }

        br.close();
        fr.close();
    }

    /**
     * Process a directory and split files into smaller ones of certain size
     */
    public void splitFilesFromDir(Path directoryPath) {
        File folder = new File(directoryPath.toString());
        File[] scholExploreFiles = folder.listFiles();

        // Parse all the files
        if (scholExploreFiles != null) {
            for (File file : scholExploreFiles) {
                try {
                    splitFile(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}