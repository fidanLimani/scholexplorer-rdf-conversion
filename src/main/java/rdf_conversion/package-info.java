/**
 * This package contains the classes that convert Java objects representing links to RDF.
 * */
package rdf_conversion;