package rdf_conversion;

import db.RDFStore;

import lombok.Data;

import model.AbstractLink;

import org.apache.jena.rdf.model.Model;

import java.util.List;

/** This class brings together the features of RDFConverter and RDFStore: Given a Link instance, convert it and store it
 * to a triple store. */
@Data
public class RDFConvertAndStore
{
    private static Model aModel;
    private RDFStore rdfStore;
    private LinkRdfConverter rdfConverter;

    /** Default constructor */
    public RDFConvertAndStore(String namedGraph) {
        rdfStore = new RDFStore(namedGraph);
    }

    public void convertAndStore(AbstractLink linkInstance) {
        rdfConverter = new LinkRdfConverter();
        // Setup and convert the link to RDF
        rdfConverter.setupPrefix();
        rdfConverter.initializeLinkInstance();
        aModel = rdfConverter.convertLink(linkInstance);

        /* A link can have 1..N providers; for this, we need to store the same Link - thus RDF model - in as many named
        graphs as there are link providers for a link. */
        List<String> namedGraphs = linkInstance.getProviders();
        for(String namedGraph : namedGraphs){
            // Substitute eventual empty space with a dash ("-");
            String uriNamedGraph = namedGraph.replace(" ", "-");

            // Store the link to Jena TDB
            rdfStore.storeModelToTDB(uriNamedGraph, aModel);
        }
    }
}