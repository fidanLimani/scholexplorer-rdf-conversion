package rdf_conversion;

import enums.ResourceType;
import model.AbstractLink;
import model.Identifier;
import model.ScholixLink;
import model.ScholixResource;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static constants.RDFConversion.*;

/** This class serializes a Link instance to RDF. */
public class LinkRdfConverter
{
    // IDation of Link instances
    private UUID uuid;

    // Create a Model instance to contain the resulting Link instance from the conversion to RDF
    private Model model;

    /** Default constructor */
    public LinkRdfConverter(){
        model = ModelFactory.createDefaultModel();
        setupPrefix();
        initializeLinkInstance();
    }

    // Classes and properties required for the RDF conversion
    Resource linkCitationClass, linkCitationInstance, linkProviderClass, alternateIdentifierClass,
            primaryResourceIdentifierClass, publicationResourceClass, datasetResourceClass, unknownResourceClass;

    Property hasCitationCreationDate, linkProviderProperty, hasIdentifier, hasLiteralValue, usesIdentifierSchemeProperty,
            hasCitingEntityProperty, hasCitationCharacterizationProperty, isCitedAsDataSourceByProperty,
            citesAsDataSourceProperty, citesAsRelatedProperty, hasCitedEntityProperty, citesForInformationProperty,
            isCitedForInformationByProperty, supplementProperty, supplementOfProperty, isRelatedToProperty;

    /**
     * Setup the model namespace
     */
    public void setupPrefix(){
        // Scholarly graph-sepcific namespaces: Links, Publications, Datasets, Authors, Concepts, etc.
        // LOGGER.info("Setting up namespace prefixes...");
        model.setNsPrefix(LINK_PREFIX, LINK);
        model.setNsPrefix(PUBLICATION_PREFIX, PUBLICATION_URI);
        model.setNsPrefix(DATASET_PREFIX, DATASET_URI);
        model.setNsPrefix(LINK_PROVIDER_PREFIX, LINK_PROVIDER);
        model.setNsPrefix(IDENTIFIER_PREFIX, IDENTIFIER);
        model.setNsPrefix(PROV_O_PREFIX, PROV_O_URI);

        // Ontologies & Vocabularies used to represent Link attributes in RDF
        model.setNsPrefix(DATA_CITE_PREFIX, DATA_CITE);
        model.setNsPrefix(CITO_PREFIX, CITO);
        model.setNsPrefix(DC_TERMS_PREFIX, DC_TERMS);
        model.setNsPrefix(RDF_PREFIX, RDF_URI);
        model.setNsPrefix(RDFS_PREFIX, RDFS_URI);
        model.setNsPrefix(XSD_PREFIX, XSD);
        model.setNsPrefix(PROV_PREFIX, PROV);
        model.setNsPrefix(BIB_PREFIX, BIB);
        model.setNsPrefix(EDM_PREFIX, EDM);
        model.setNsPrefix(LITERAL_PREFIX, LITERAL);
        model.setNsPrefix(FRBR_PREFIX, FRBR_URI);
    }

    /**
     * Initialize the RDF model and required namespaces, classes, and property instances.
     * Use rdfs:label whenever possible as it is well-supported by client applications;
     * <Note>
        - Resources created by ResourceFactory will not refer to any model and will not permit operations that
     * require a model. Such resources are useful as general constants.
     *
        - The fields defined in this method are initialized in order to have a smaller memory (and processing)
     * footprint when converting millions of Link instances for each file. A better programming practice could be to
     * introduce fields as closer to the point of their use as possible .
     * </Note>
     */
    public void initializeLinkInstance () {
        // LOGGER.info("Initializing classes and properties for the RDF model...");
        linkCitationClass = ResourceFactory.createResource(CITO + A_CITATION);
        hasCitationCreationDate = ResourceFactory.createProperty(CITO, HAS_CITATION_CREATION_DATE);
        linkProviderClass = ResourceFactory.createResource(EDM + EDM_AGENT);
        linkProviderProperty = ResourceFactory.createProperty(EDM, EDM_PROVIDER);

        // Identifier-related
        primaryResourceIdentifierClass = ResourceFactory.createResource(DATA_CITE + PRIMARY_RESOURCE_ID);
        alternateIdentifierClass = ResourceFactory.createResource(DATA_CITE + ALTERNATE_RESOURCE_ID);
        hasIdentifier = ResourceFactory.createProperty(DATA_CITE, PROPERTY_HAS_IDENTIFIER);
        usesIdentifierSchemeProperty = ResourceFactory.createProperty(DATA_CITE, USES_IDENTIFIER_SCHEME);

        // Relation type-related
        hasCitingEntityProperty = ResourceFactory.createProperty(CITO, HAS_CITING_ENTITY);
        hasCitationCharacterizationProperty = ResourceFactory.createProperty(CITO, HAS_CITATION_CHARACTERIZATION);
        isCitedAsDataSourceByProperty = ResourceFactory.createProperty(CITO, IS_CITED_AS_DATA_SOURCE_BY);
        citesAsDataSourceProperty = ResourceFactory.createProperty(CITO, CITES_AS_DATA_SOURCE);
        citesAsRelatedProperty = ResourceFactory.createProperty(CITO, CITES_AS_RELATED);
        hasCitedEntityProperty = ResourceFactory.createProperty(CITO, HAS_CITED_ENTITY);
        citesForInformationProperty = ResourceFactory.createProperty(CITO, CITES_FOR_INFORMATION);
        isCitedForInformationByProperty = ResourceFactory.createProperty(CITO, IS_CITED_FOR_INFORMATION_BY);
        supplementProperty = ResourceFactory.createProperty(FRBR_URI, SUPPLEMENT);
        supplementOfProperty = ResourceFactory.createProperty(FRBR_URI, SUPPLEMENT_OF);
        isRelatedToProperty = ResourceFactory.createProperty(EDM, IS_RELATED_TO);

        // Other classes and properties
        hasLiteralValue = ResourceFactory.createProperty(DATA_CITE, PROPERTY_HAS_LITERAL_VALUE);
        publicationResourceClass = ResourceFactory.createResource(BIB + PUBLICATION_LABEL);
        datasetResourceClass = ResourceFactory.createResource(BIB + DATASET_LABEL);
        unknownResourceClass = ResourceFactory.createResource(UNKNOWN_URI + UNKNOWN_LABEL);
    }

    /**
     * Convert Link instance: For every optional element, check whether it is present before attempting an operation on it.
     * @param link Link instance
     */
    public Model convertLink(AbstractLink link) {
        // The model to store all link-related statements, and return it to the invoking party
        Model tempModel = ModelFactory.createDefaultModel();
        ScholixLink scholixLink = (ScholixLink) link;
        Resource sourceObject, targetObject;

        // Means to identify all Link instances
        uuid = UUID.randomUUID();

        // Link attributes: Instance, Publication date (1)
        linkCitationInstance = model.createResource(LINK + uuid)
                                            .addProperty(RDF.type, linkCitationClass)
                                            .addProperty(hasCitationCreationDate, // instead of "DCTerms.date"
                                                        ResourceFactory.createTypedLiteral(
                                                                    scholixLink.getPublicationDate().toString(),
                                                                                    XSDDatatype.XSDdate));

        // Link provider (1..N): Substitute possible empty space with a dash, and lowercase it (just to look more "normal" URI)
        List<String> linkProviders = scholixLink.getProviders();
        for(String linkProvider : linkProviders) {
            String processedLinkProvider = linkProvider.replace(" ", "-").toLowerCase();

            // Link provider (1..N) Create the link provider instance
            Resource linkProviderInstance = model.createResource(LINK_PROVIDER + processedLinkProvider)
                                                .addProperty(RDF.type, linkProviderClass)
                                                .addProperty(RDFS.label, linkProvider);

            // Add each link provider to the link citation instance
            linkCitationInstance.addProperty(linkProviderProperty, linkProviderInstance);
        }

        // License URL (O..1): This is an optional attribute! addLiteral(DCTerms.license, ModelConstants.LINK_LICENSE)
        String licenseURL = scholixLink.getLicenseURL();
        if((licenseURL != null) && (!licenseURL.isEmpty())){
            linkCitationInstance.addProperty(DCTerms.license, ResourceFactory
                                                            .createTypedLiteral(licenseURL, XSDDatatype.XSDstring));
        }

        // Convert source
        sourceObject = convertResource((ScholixResource) scholixLink.getSourceObject());

        // Convert target: Change the uuid value to distinguish b/w source and target object IDers
        targetObject = convertResource((ScholixResource) scholixLink.getTargetObject());

        // Relationship type (1)
        String relationType = scholixLink.getRelationType().name();

        switch(relationType){
            // References -> cito:citesForInformation
            case "References":
                sourceObject.addProperty(citesForInformationProperty, targetObject);
                break;
            // IsReferencedBy -> cito:isCitedForInformationBy
            case "IsReferencedBy":
                sourceObject.addProperty(isCitedForInformationByProperty, targetObject);
                break;
            // IsSupplementedBy -> frbr:supplement
            case "IsSupplementedBy":
                sourceObject.addProperty(supplementProperty, targetObject);
                break;
            // IsSupplementTo -> frbr:supplementOf
            case "IsSupplementTo":
                sourceObject.addProperty(supplementOfProperty, targetObject);
                break;
            // IsRelatedTo -> edm: isRelatedTo
            case "IsRelatedTo":
                sourceObject.addProperty(isRelatedToProperty, targetObject);
                break;
            default:
                System.out.println("No match!");
                break;
        }

        // Add source and target objects to the Link instance
        linkCitationInstance.addProperty(hasCitingEntityProperty, sourceObject)
            .addProperty(hasCitedEntityProperty, targetObject);

        String strSourceObjectType = ((ScholixLink) link).getSourceObject().getType().name();
        String strTargetObjectType = ((ScholixLink) link).getTargetObject().getType().name();

        // literature-to-dataset links
        if(strSourceObjectType.equalsIgnoreCase(ResourceType.LITERATURE.name()) &&
                strTargetObjectType.equalsIgnoreCase(ResourceType.DATASET.name())){
            linkCitationInstance.addProperty(hasCitationCharacterizationProperty, citesAsDataSourceProperty);
        }

        //   cito:hasCitationCharacterization cito:isCitedAsDataSourceBy (data-to-publication link)
        if(strSourceObjectType.equalsIgnoreCase(ResourceType.DATASET.name()) &&
                strTargetObjectType.equalsIgnoreCase(ResourceType.LITERATURE.name())){
            linkCitationInstance.addProperty(hasCitationCharacterizationProperty, isCitedAsDataSourceByProperty);
        }

        // dataset-to-dataset links: cito:hasCitationCharacterization cito:iscitesAsRelated (data-to-data link)
        if(strSourceObjectType.equalsIgnoreCase(ResourceType.DATASET.name()) &&
                strTargetObjectType.equalsIgnoreCase(ResourceType.DATASET.name())){
            linkCitationInstance.addProperty(hasCitationCharacterizationProperty, citesAsRelatedProperty);
        }

        /* Store the RDF representation to a named graph (passed to the method) */
        tempModel.add(model);
        // writeModelToFile("src/main/java/file_utils/linkInstance2.ttl", model);
        /* Is it necessary to clear the contents of the Model instance to ready it for the next Link instance conversion? */
        // tempModel.write(System.out, "TURTLE");

        model.removeAll(); // Ready the model for the next Link instance conversion
        /*
        System.out.println("What is in <model> after invoking removeAll()?\n");
        model.write(System.out, "TURTLE");
        */

        return tempModel;
    }

    /**
     * Converts the source or target of the link to RDF
     * @param resource The resource - source or target object - of a Link instance
     * @return Resource
     */
    public Resource convertResource(ScholixResource resource){
        // An ID for the source or target object and their attributes
        uuid = UUID.randomUUID();

        // A Resource instance to contain the source or target object of the Link instance
        Resource resourceObjectInstance;
        ResourceType resourceType = resource.getType();

        // 1. Object Type
        if(resourceType.toString().equalsIgnoreCase("literature")){
            // Create a publication instance
            resourceObjectInstance = model.createResource(PUBLICATION_URI + uuid)
                    .addProperty(RDF.type, publicationResourceClass);
        } else if (resourceType.toString().equalsIgnoreCase("dataset")){
            // Create a dataset instance
            resourceObjectInstance = model.createResource(DATASET_URI + uuid)
                    .addProperty(RDF.type, datasetResourceClass);
        } else {
            // Create a resource of "unknown" instance
            resourceObjectInstance = model.createResource(UNKNOWN_URI + uuid)
                    .addProperty(RDF.type, unknownResourceClass);
        }

        /* 2. Identifier (1)
         * There are resources based on DOI (datacite:PrimaryResourceIdentifier) and others that do not
         * (datacite:AlternateResourceIdentifier). */
        Identifier identifier = resource.getIdentifier();
        Resource resourceIdentifierInstance;

        // datacite:PrimaryResourceIdentifier for Identifiers of scheme DOI.
        if(identifier.getIdScheme().equalsIgnoreCase("doi")){
            resourceIdentifierInstance = model.createResource(IDENTIFIER + uuid.toString())
                    .addProperty(RDF.type, primaryResourceIdentifierClass);
        } else {
            resourceIdentifierInstance = model.createResource(IDENTIFIER + uuid.toString())
                    .addProperty(RDF.type, alternateIdentifierClass);
       }

        // Add the rest of the Identifer properties, such as shcema name or schema URL;
        resourceIdentifierInstance.addProperty(usesIdentifierSchemeProperty, identifier.getIdScheme());

        // Complete Identifier instance with the literal value
        resourceIdentifierInstance.addProperty(hasLiteralValue, identifier.getId());

        // 2. Object identifier (1)
        resourceObjectInstance.addProperty(hasIdentifier, resourceIdentifierInstance);

         /*
            For every optional element, check whether it is present before attempting to add it to the model:
            Optional resources for Source/Target object: Title, Publisher, Creator, Publication date
        */
        // 3. Object title (0..1)
        String title = resource.getTitle();
        if((title != null) && (!title.isEmpty())){
            resourceObjectInstance.addProperty(DCTerms.title, ResourceFactory
                    .createTypedLiteral(title, XSDDatatype.XSDstring));
        }

        // 4. Object Publisher (0..1): Create an foaf:Agent to represent both a publisher and a creator?
        String publisher = resource.getPublisher();
        if((publisher != null) && (!publisher.isEmpty())){
                resourceObjectInstance.addProperty(DCTerms.publisher,
                        ResourceFactory.createTypedLiteral(publisher, XSDDatatype.XSDstring));
        }

        // 5. Object Creator (0..N)
        List<String> creators = resource.getAuthors();
        if((creators != null) && (creators.size() > 0)){
            for(String creator : creators) {
                resourceObjectInstance.addProperty(DCTerms.creator,
                        ResourceFactory.createTypedLiteral(creator, XSDDatatype.XSDstring));
            }
        }

        // 6. Object Publication Date (0..1)
        LocalDate publicationDate = resource.getPublicationDate();
        if(publicationDate != null){
            resourceObjectInstance.addProperty(DCTerms.date,
                    ResourceFactory.createTypedLiteral(publicationDate.toString(), XSDDatatype.XSDdate));
        }

        return resourceObjectInstance;
    }

    /**
     * This method adds the provenance of the model, and it is invoked once the complete ScholExplorer collection
     * has been converted to RDF.
     * @param startedAt The time the conversion process starts at
     * @param endedAt The time the conversion process ends at (for the whole collection)
     * @return Model instance that contains ScholExplorer collection provenance
     */
    public Model createProvenanceModel(Instant startedAt, Instant endedAt) {
        // A Model instance to store the provenance
        Model provModel = ModelFactory.createDefaultModel();
        Resource provCollectionClass, provCollectionInstance, provGenerationClass, provGenerationInstance,
                provActivityClass, provSoftwareAgentClass, provSoftwareAgentInstance, rdfLinkCollectionInstance,
                provActivityInstance;
        Property provHadMemberProperty, provGenerationAtTime, provGenerationActivityProperty, activityStartedAtProperty,
                activityEndedAtProperty, activityGeneratedProperty, provUsedProperty;

        // Initialize required classes, properties, and instances
        provCollectionClass = ResourceFactory.createResource(PROV + PROV_COLLECTION_CLASS);
        provHadMemberProperty = ResourceFactory.createProperty(PROV, PROV_HAD_MEMBER_PROPERTY);
        provGenerationClass = ResourceFactory.createResource(PROV + PROV_GENERATION_CLASS);
        provGenerationAtTime = ResourceFactory.createProperty(PROV, PROV_AT_TIME_PROPERTY);
        provGenerationActivityProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_PROPERTY);
        provActivityClass = ResourceFactory.createResource(PROV + PROV_ACTIVITY_CLASS);
        provSoftwareAgentClass = ResourceFactory.createResource(PROV + PROV_SOFTWARE_AGENT_CLASS);
        activityStartedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_START_TIME_VALUE);
        activityEndedAtProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_END_TIME_VALUE);
        activityGeneratedProperty = ResourceFactory.createProperty(PROV, PROV_GENERATED_VALUE);
        provUsedProperty = ResourceFactory.createProperty(PROV, PROV_USED_VALUE);

        // ProvGeneration instance
        provGenerationInstance = provModel.createResource(PROVENANCE_URI + PROV_GENERATION_INSTANCE_VALUE);

        // ScholExplorer in RDF representation
        // :conversion-workflow: The software workflow (SoftwareAgent) that conducts the conversion
        Literal provGenerationComment = ResourceFactory.createLangLiteral(PROV_GENERATION_COMMENT_VALUE,
                LANGUAGE_TAG_EN);
        provSoftwareAgentInstance = provModel.createResource(PROVENANCE_URI + PROV_ACTIVITY_INSTANCE)
            .addProperty(RDF.type, provSoftwareAgentClass);

        //:scholexplorer-link-collection -> The original ScholExplorer collection
        provCollectionInstance =
                provModel.createResource(PROVENANCE_URI + ORIGINAL_COLLECTION_INSTANCE)
                    .addProperty(RDF.type, provCollectionClass)
                    .addProperty(DCTerms.title, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_TITLE,
                            XSDDatatype.XSDstring))
                    .addProperty(DCTerms.description, ResourceFactory
                            .createTypedLiteral(ORIGINAL_COLLECTION_DESCRIPTION, XSDDatatype.XSDstring))
                    .addProperty(DCTerms.issued, ResourceFactory.createTypedLiteral(LocalDate.now().toString(),
                            XSDDatatype.XSDdate))
                    .addProperty(DCTerms.format, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_FORMAT,
                            XSDDatatype.XSDstring))
                    .addProperty(DCTerms.identifier,
                            ResourceFactory.createTypedLiteral(ORIGNAL_COLLECTOIN_URI, XSDDatatype.XSDanyURI));

        /* Option 1: List the files part of the ScholExplorer and add them as part of the collection via "prov:hadMember"
        * Note: This option, when included in the provenance model, triggers an issue with the path delimiter from Windows.
        * Jena TDB loader complains about an "s" character not being in a hexadecimal format.
        * */
        File[] files = new File(ORIGINAL_COLLECTION_LOCAL_PATH).listFiles();
        if (files != null) {
            for (File file : files){
                provCollectionInstance.addProperty(provHadMemberProperty, ResourceFactory.createResource(String.format(ORIGINAL_COLLECTION_FILE,
                        file.getAbsolutePath().replace("\\", "/").replace(" ", "%20"))));
            }
        }

        // Option 2: List individual ScholExplorer files being converted part via "prov:hadMember"
        /*
        provCollectionInstance.addProperty(provHadMemberProperty, ResourceFactory.createResource(String.format(ORIGINAL_COLLECTION_FILE,
                fileName)));
         */

        Property provWasGeneratedByProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_WAS_GENERATED_BY_VALUE);
        Property provQualifiedGenerationProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_QUALIFIED_GENERATION_VALUE);
        rdfLinkCollectionInstance = provModel.createResource(PROVENANCE_URI + RDF_COLLECTION_INSTANCE)
                .addProperty(RDF.type, provCollectionClass)
                .addProperty(provWasGeneratedByProperty, provSoftwareAgentInstance)
                .addProperty(provQualifiedGenerationProperty, provGenerationInstance);

        provActivityInstance = provModel.createResource(PROVENANCE_URI + LINK_CONVERSION_VALUE)
            .addProperty(RDF.type, provActivityClass)
            .addProperty(activityStartedAtProperty, ResourceFactory.createTypedLiteral(startedAt.toString(), XSDDatatype.XSDstring))
            .addProperty(provUsedProperty, provCollectionInstance)
            .addProperty(activityGeneratedProperty, rdfLinkCollectionInstance)
            .addProperty(activityEndedAtProperty, ResourceFactory.createTypedLiteral(endedAt.toString(), XSDDatatype.XSDstring));

        // ScholExplorer in RDF representation
        provGenerationInstance
                .addProperty(RDF.type, provGenerationClass)
                .addProperty(provGenerationAtTime, LocalDate.now().toString())
                .addProperty(RDFS.comment, provGenerationComment)
                .addProperty(provGenerationActivityProperty, provActivityInstance);

        return provModel;
    }

    /**
     * This method adds the provenance of the model, and it is invoked once the complete ScholExplorer collection
     * has been converted to RDF.
     * @return Model instance that contains ScholExplorer collection provenance
     */
    public Model createProvenanceModel() {
        // A Model instance to store the provenance
        Model provModel = ModelFactory.createDefaultModel();
        Resource provCollectionClass, provCollectionInstance, provGenerationClass, provGenerationInstance,
                provActivityClass, provSoftwareAgentClass, provSoftwareAgentInstance, rdfLinkCollectionInstance,
                provActivityInstance;
        Property provHadMemberProperty, provGenerationAtTime, provGenerationActivityProperty, activityGeneratedProperty,
                provUsedProperty;

        // Initialize required classes, properties, and instances
        provCollectionClass = ResourceFactory.createResource(PROV + PROV_COLLECTION_CLASS);
        provHadMemberProperty = ResourceFactory.createProperty(PROV, PROV_HAD_MEMBER_PROPERTY);
        provGenerationClass = ResourceFactory.createResource(PROV + PROV_GENERATION_CLASS);
        provGenerationAtTime = ResourceFactory.createProperty(PROV, PROV_AT_TIME_PROPERTY);
        provGenerationActivityProperty = ResourceFactory.createProperty(PROV, PROV_ACTIVITY_PROPERTY);
        provActivityClass = ResourceFactory.createResource(PROV + PROV_ACTIVITY_CLASS);
        provSoftwareAgentClass = ResourceFactory.createResource(PROV + PROV_SOFTWARE_AGENT_CLASS);
        activityGeneratedProperty = ResourceFactory.createProperty(PROV, PROV_GENERATED_VALUE);
        provUsedProperty = ResourceFactory.createProperty(PROV, PROV_USED_VALUE);

        // ProvGeneration instance
        provGenerationInstance = provModel.createResource(PROVENANCE_URI + PROV_GENERATION_INSTANCE_VALUE);

        // ScholExplorer in RDF representation
        // :conversion-workflow: The software workflow (SoftwareAgent) that conducts the conversion
        Literal provGenerationComment = ResourceFactory.createLangLiteral(PROV_GENERATION_COMMENT_VALUE,
                LANGUAGE_TAG_EN);
        provSoftwareAgentInstance = provModel.createResource(PROVENANCE_URI + PROV_ACTIVITY_INSTANCE)
                .addProperty(RDF.type, provSoftwareAgentClass);

        //:scholexplorer-link-collection -> The original ScholExplorer collection
        provCollectionInstance =
                provModel.createResource(PROVENANCE_URI + ORIGINAL_COLLECTION_INSTANCE)
                        .addProperty(RDF.type, provCollectionClass)
                        .addProperty(DCTerms.title, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_TITLE,
                                XSDDatatype.XSDstring))
                        .addProperty(DCTerms.description, ResourceFactory
                                .createTypedLiteral(ORIGINAL_COLLECTION_DESCRIPTION, XSDDatatype.XSDstring))
                        .addProperty(DCTerms.issued, ResourceFactory.createTypedLiteral(LocalDate.now().toString(),
                                XSDDatatype.XSDdate))
                        .addProperty(DCTerms.format, ResourceFactory.createTypedLiteral(ORIGINAL_COLLECTION_FORMAT,
                                XSDDatatype.XSDstring))
                        .addProperty(DCTerms.identifier,
                                ResourceFactory.createTypedLiteral(ORIGNAL_COLLECTOIN_URI, XSDDatatype.XSDanyURI));

        /* Option 1: List the files part of the ScholExplorer and add them as part of the collection via "prov:hadMember"
         * Note: This option, when included in the provenance model, triggers an issue with the path delimiter from Windows.
         * Jena TDB loader complains about an "s" character not being in a hexadecimal format.
         * */
        File[] files = new File(ORIGINAL_COLLECTION_LOCAL_PATH).listFiles();
        if (files != null) {
            for (File file : files){
                provCollectionInstance.addProperty(provHadMemberProperty, ResourceFactory
                        .createResource(String.format(ORIGINAL_COLLECTION_FILE,
                                                        file.getAbsolutePath().replace("\\", "/")
                                                                                .replace(" ", "%20"))));
            }
        }

        Property provWasGeneratedByProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_WAS_GENERATED_BY_VALUE);
        Property provQualifiedGenerationProperty = ResourceFactory.createProperty(PROV_O_URI, PROV_QUALIFIED_GENERATION_VALUE);
        rdfLinkCollectionInstance = provModel.createResource(PROVENANCE_URI + RDF_COLLECTION_INSTANCE)
                .addProperty(RDF.type, provCollectionClass)
                .addProperty(provWasGeneratedByProperty, provSoftwareAgentInstance)
                .addProperty(provQualifiedGenerationProperty, provGenerationInstance);

        provActivityInstance = provModel.createResource(PROVENANCE_URI + LINK_CONVERSION_VALUE)
                .addProperty(RDF.type, provActivityClass)
                .addProperty(provUsedProperty, provCollectionInstance)
                .addProperty(activityGeneratedProperty, rdfLinkCollectionInstance);

        // ScholExplorer in RDF representation
        provGenerationInstance
                .addProperty(RDF.type, provGenerationClass)
                .addProperty(provGenerationAtTime, LocalDate.now().toString())
                .addProperty(RDFS.comment, provGenerationComment)
                .addProperty(provGenerationActivityProperty, provActivityInstance);

        return provModel;
    }

    /**
     * Write the RDF model to a file. This is to test the conversion process with individual Link instances.
     * @param filePath The file to store the model contents to
     */
    private void writeModelToFile (String filePath, Model model) {
        // LOGGER.info("Writing RDF model to a store...");

        FileOutputStream fout;
        try {
            fout = new FileOutputStream(filePath);
            model.write(fout, "TURTLE");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Test the app
    public static void main(String[] args){
        String outputRdfFile = "src/main/resources/linkInstance2.ttl";
        AbstractLink testLink = ScholixLink.generateTestLink();

        // Conversion process starts (used for dataset provenance)
        Instant startedAt = Instant.now();

        LinkRdfConverter linkRdfConverter = new LinkRdfConverter();

        // 1. Setup namespace prefix
        linkRdfConverter.setupPrefix();

        // 2. Initialize classes, properties, and instances required for the conversion
        linkRdfConverter.initializeLinkInstance();

        // 3. Convert a Link instance to RDF
        Model aModel = linkRdfConverter.convertLink(testLink);

        // 4. Conversion process ends (used for dataset provenance), part of a separate named graph;
        Instant endedAt = Instant.now();
        aModel.add(linkRdfConverter.createProvenanceModel(startedAt, endedAt));

        // 5. Store model: Write it to a file or store it in a triple store
        linkRdfConverter.writeModelToFile(outputRdfFile, aModel);
    }
}