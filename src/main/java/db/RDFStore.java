package db;

import lombok.Data;
import org.apache.jena.dboe.base.file.Location;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.system.Txn;
import org.apache.jena.tdb2.DatabaseMgr;
import org.apache.jena.tdb2.TDB2Factory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static constants.DBConstants.SCHOL_LINK_DATASET;
import static constants.HarvestLogging.SCHOL_EXPLORER_OUTPUT;

/**
 * 1. load a dataset on a TDB
 * 2. Store a Model instance in a named graph
 * 3. List named graphs in a dataset
 * 4. Query named graphs or the union of all graphs stored in a TDB-backed dataset
 */
@Data
public class RDFStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(RDFStore.class);
    private Dataset dataset;
    private Location location;

    /** Constructor: Initialize the dataset field to a @code{Location} instance;
     * @param datasetName The name of the dataset the TDB needs to load (or be backed on)
     */
    public RDFStore(String datasetName) {
        location = Location.create(String.format(SCHOL_EXPLORER_OUTPUT, datasetName));
        dataset = TDB2Factory.connectDataset(location);
        // LOGGER.info("Jena TDB loaded based on the dataset: " + location.getDirectoryPath());
    }

    /** Store a model in a TDB named graph
     * @param namedGraph Link instance provider represents the named graph we store this instance in;
     * @param model RDF representation of the Link instance, stored in an in-memory (RDF) model;
     */
    public void storeModelToTDB(String namedGraph, Model model) {
        Txn.executeWrite(dataset, ()-> dataset.addNamedModel(namedGraph, model));
    }

    /** Store a model in TDB's default graph
     * @param model RDF representation of the Link instance, stored in an in-memory (RDF) model;
     */
    public void storeLinkInstanceInDefaultGraph(Model model) {
        Txn.executeWrite(dataset, ()-> dataset.getDefaultModel().add(model));
    }

    /** Store a model in a file
     * @param storageFile The file to store to
     * @param model The Model instance to store
     * @param rdfFormat The format of storing the Model instance
     */
    public void storeModelToFile(String storageFile, Model model, RDFFormat rdfFormat) {
        String fileOutputPath = "src/main/resources/output/%s";
        DatasetGraph datasetGraph = getDataset().asDatasetGraph();

        try (FileOutputStream fos = new FileOutputStream(String.format(fileOutputPath, storageFile))) {
            // Stream it to a file
            RDFDataMgr.write(fos, datasetGraph, rdfFormat);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove a named graph
     * @param namedGraph The named graph
     */
    public void removeNamedGraph(String namedGraph) {
        String queryString = String.format("CLEAR GRAPH <%s>", namedGraph);
        String queryString2 = String.format("DELETE WHERE { Graph <%s> { ?s ?p ?o . } }", namedGraph);

        dataset.begin(ReadWrite.WRITE);
        try {
            UpdateRequest uReq = UpdateFactory.create(queryString2);
            UpdateExecutionFactory.create(uReq, dataset).execute();
            dataset.commit();
        } finally {
            dataset.end();
        }
    }

    /** Remove all named graph */
    public void removeAllNamedGraph(){
        dataset.begin(ReadWrite.WRITE);
        try {
            Iterator<String> datasetNamesIterator = dataset.listNames();
            while (datasetNamesIterator.hasNext()) {
                dataset.removeNamedModel(datasetNamesIterator.next());
            }

            dataset.commit();
        } finally {
            dataset.end();
        }
    }

    /** Remove all triples from a named graph */
    public void emptyNamedGraph(String namedModel){
        System.out.printf("Emptying the model <%s>\n", namedModel);

        dataset.begin(ReadWrite.WRITE);
        try {
            dataset.getNamedModel(namedModel).removeAll();
            dataset.commit();
        } finally {
            dataset.end();
        }
    }

    /** A method to query a Jena model */
    public void queryDatasetModel(String queryString, File file){
        dataset.begin(ReadWrite.READ);
        try {
            try(QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset)) {
                ResultSet rs = qExec.execSelect() ;
                // ResultSetFormatter.out(rs);

                // Store the result in a file
                FileIO.appendToFile(ResultSetFormatter.asText(rs), file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            dataset.commit();
        } finally {
            dataset.end();
        }
    }

    /**
     * List some TDB-related aspects:
     * @param file The file to contain a SPARQL query result
     */
    public void generateDatasetStats(File file) {
        LOGGER.info("Query the RDF ScholExplorer collection and generate some statistics about the dataset...");

        // Is a dataset backed by a TDB?
        // System.out.println("Is this dataset a TDB-backed one? " + TDB2Factory.isTDB2(dataset));

        // Which queries are we interested to have for the ScholExplore dataset?

        // List named graphs of the dataset
        // System.out.println("List named graphs of the dataset");
        /*
        String queryString0 = "SELECT Distinct ?namedGraph { GRAPH ?namedGraph {}}";
        queryDatasetModel(queryString0, file);
         */

        // List all triples across named graphs
        /*
        String queryString1 = "SELECT * { GRAPH <http://zbw.eu/scholarly-resources/graph/provenance/> {?s ?p ?o . }}";
        queryDatasetModel(queryString1, file);
         */

        // List # of triples for certain named graph
        /*
        String queryString2 =
                "Select (Count(*) As ?tripleCount)\n" +
                        "From <http://zbw.eu/scholarly-resources/graph/Data-Cite>\n" +
                        "Where { ?s ?p ?o . }";
         */

        // # of triples in each named graph
        // System.out.println("# of triples in each named graph");
        String queryString3 = "Select (Count(*) As ?triplesPerGraph) ?graph {\n" +
                "{ Graph ?graph { ?s ?p ?o . } } }" +
                "Group By ?graph";
        queryDatasetModel(queryString3, file);

        // System.out.println("# of triples in the default graph");
        // Used when triples are stored in the <default graph>
        /*
        String queryString3a = "Select (Count(*) As ?totalTriples)\n" + "{ ?s ?p ?o . }  ";
        queryDatasetModel(queryString3a, file);
         */

        // Total # of triples
        // System.out.println("Total # of triples");
        /*
        String queryString3a = "Select (Count(*) As ?totalTripleCount) {\n" +
                "{ Graph ?graph { ?s ?p ?o . } } }";
        queryDatasetModel(queryString3a, file);
         */

        // Count link instances (cito:Citation) per named graphs.
        // System.out.println("Count link instances (cito:Citation) per named graphs.");
        String queryString4 = "Prefix cito: <http://purl.org/spar/cito/>\n" +
                "Select (Count(*) As ?totalLinks) ?graph " +
                "Where { " +
                "   Graph ?graph { ?s a cito:Citation . } " +
                "} " +
                "Group By ?graph";
        queryDatasetModel(queryString4, file);

        // Count the total # of link instances (cito:Citation)
        /* Unnecessary especially since we already generate the # of links per named graph
        String queryString4b = "Prefix cito: <http://purl.org/spar/cito/>\n" +
                "Select (Count(*) As ?totalLinks) " +
                "Where { " +
                "   Graph ?graph { ?s a cito:Citation . } }";
        queryDatasetModel(queryString4b, file);
        */

        // # of dataset instances across named graphs
        // System.out.println("# of dataset instances across named graphs");
        String queryString5 =
                "Prefix bib: <http://id.loc.gov/ontologies/bibframe/>\n" +
                        "Select (Count(?s) As ?totalDatasets) " +
                        "Where { " +
                        "   Graph ?graph { ?s a bib:Dataset . }" +
                        "}";
        queryDatasetModel(queryString5, file);

        // # of publication instances across named graphs
        // System.out.println("# of publication instances across named graphs");
        String queryString6 =
                "Prefix bib: <http://id.loc.gov/ontologies/bibframe/>\n" +
                        "Select (Count(?s) As ?totalPublications) " +
                        "WHERE { " +
                        "   GRAPH ?graph { ?s a bib:Publication . }" +
                        "}";
        queryDatasetModel(queryString6, file);

        // Obtain the list of classes
        // System.out.println("Obtain the list of classes");
        /*
        String queryString7 =
                "Select Distinct ?usedClasses " +
                        "Where { Graph ?graph { ?s a ?usedClasses .} " +
                        "}";
        queryDatasetModel(queryString7, file);
        */

        // What is the average # of triples per Link instance (across named graphs)?
        // System.out.println("What is the average # of triples per Link instance (across named graphs)?");
        /*
        String queryString8 = "Prefix cito: <http://purl.org/spar/cito/>\n" +
                "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                "SELECT (Count(*) AS ?totalTriples) ?totalLinks (xsd:integer(Count(?s)) / ?totalLinks AS ?average) " +
                "Where { " +
                "   Graph ?graph { ?s ?p ?o . } " +
                "   { " +
                "    \tSelect (Count(*) As ?totalLinks) " +
                "        Where { Graph ?graph { ?s a cito:Citation . } } " +
                "    } " +
                "} Group By ?totalLinks";
        queryDatasetModel(queryString8, file);
         */
    }

    /**
     * This method creates a back up of the Jena TDB. It includes
     */
    public void backupAndCompressDataset() {
        DatasetGraph dsg = DatabaseMgr.connectDatasetGraph(location);

        // DatabaseMgr.backup(dsg) returns a String of the location where the back up file is stored (if we need it).
        DatabaseMgr.backup(dsg);
    }

    /**
     * Test method to query the dataset: Find the average triples / link instance
     */
    public void testQueryDatasetModel(String queryString){
        dataset.begin(ReadWrite.READ);
        try {
            try(QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset)) {
                ResultSet rs = qExec.execSelect() ;
                ResultSetFormatter.out(rs);
            }

            dataset.commit();
        } finally {
            dataset.end();
        }
    }

    // Test the app
    public static void main(String... args) {
        RDFStore rdfStore = new RDFStore(SCHOL_LINK_DATASET);
        File aFile = new File(String.format(SCHOL_EXPLORER_OUTPUT, "testSparqlFile.txt"));
        // rdfStore.removeAllNamedGraph(); // On and off: to check the conversion to RDF

        // Query the dataset
        rdfStore.generateDatasetStats(aFile);
        // http://localhost:3030/scholExplorer/data/testGraph

        // rdfStore.listDatasetStats();
        // rdfStore.removeNamedGraph("econbiz");

        // Test transaction processing with Jena TDB: storeLinkInstance(Map<String, Model> graphModelMap)
        /*
        ScholixLink testLink = (ScholixLink) ScholixLink.generateTestLink();
        Map<String, Model> testModelMap = new HashMap<>();
        RdfConverter aConverter = new RdfConverter();
        aConverter.setupPrefix();
        aConverter.initializeLinkInstance();
        Model testModel = aConverter.convertLink(testLink);
        testModelMap.put("Data cite", testModel);
        rdfStore.storeLinkInstance(testModelMap);
        */
    }
}