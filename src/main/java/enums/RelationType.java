package enums;

/**
 * This enumeration represents the description of the relationship of the resource being registered (A)
 * and the related resource (B).
 *
 * Source: Mainly terms from DataCite v4.0; CiTO ontology and another term found in ScholExplore collection are also
 * represented in this list.
 */

public enum RelationType {
    // The first two enumerations are from CiTO Ontology
    /**
     * Indicates that publication A cites dataset B as a data source.
     */
    CitesAsDataSource,

    /**
     * Indicates that dataset B is cited by publication A as a data source.
     */
    IsCitedAsDataSourceBy,

    /**
     * Indicates that dataset A is related to dataset B.
     */
    CitesAsRelated,

    /**
     * Indicates that B includes A in a citation.
     */
    IsCitedBy,

    /**
     * Indicates that A includes B in a citation.
     */
    Cites,

    /**
     * Indicates that A is a supplement to B.
     */
    IsSupplementTo,

    /**
     * Indicates that B is a supplement to A.
     */
    IsSupplementedBy,

    /**
     * Indicates A is continued by the work B.
     */
    IsContinuedBy,

    /**
     * Indicates A is a continuation of the work B.
     */
    Continues,

    /**
     * Indicates A describes B.
     */
    Describes,

    /**
     * Indicates A is described by B.
     */
    IsDescribedBy,

    /**
     * Indicates resource A has additional metadata B.
     */
    HasMetadata,

    /**
     * Indicates additional metadata A for a resource B.
     */
    IsMetadataFor,

    /**
     * Indicates A has a version B.
     */
    HasVersion,

    /**
     * Indicates A is a version of B.
     */
    IsVersionOf,

    /**
     * Indicates A is a new edition of B, where the new edition has been modified or updated.
     */
    IsNewVersionOf,

    /**
     * Indicates A is a previous edition of B.
     */
    IsPreviousVersionOf,

    /**
     * Indicates A is a portion of B; may be used for elements of a series.
     */
    IsPartOf,

    /**
     * Indicates A includes the part B.
     */
    HasPart,

    /**
     * Indicates A is used as a source of information by B.
     */
    IsReferencedBy,

    /**
     * Indicates B is used as a source of information for A.
     */
    References,

    /**
     * Indicates B is documentation about/explaining A.
     */
    IsDocumentedBy,

    /**
     * Indicates A is documentation about/explains B.
     */
    Documents,

    /**
     * Indicates B is used to compile or create A.
     */
    IsCompiledBy,

    /**
     * Indicates B is the result of a compile or creation event using A.
     */
    Compiles,

    /**
     * Indicates A is a variant or different form of B, e.g. calculated or calibrated form or different packaging.
     */
    IsVariantFormOf,

    /**
     * Indicates A is the original form of B.
     */
    IsOriginalFormOf,

    /**
     * Indicates that A is identical to B, for use when there is a need to register two separate instances of the same resource.
     */
    IsIdenticalTo,

    /**
     * Indicates that A is reviewed by B.
     */
    IsReviewedBy,

    /**
     * Indicates that A is a review of B.
     */
    Reviews,

    /**
     * Indicates B is a source upon which A is based.
     */
    IsDerivedFrom,

    /**
     * Indicates A is a source upon which B is based.
     */
    IsSourceOf,

    /**
     * Indicates A is required by B.
     */
    IsRequiredBy,

    /**
     * Indicates A requires B.
     */
    Requires,

    /**
     * Indicates A is related to B (in lack of a more precise relationship)
     * Note: Present in ScholExplore link collection.
     */
    IsRelatedTo,

    /**
     * Indicates that the relationship between the resources is unknown
     */
    Unknown
}