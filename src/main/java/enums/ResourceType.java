package enums;

/**
 * This enumeration represents the description of resource being linked.
 *
 * Source: Adopted based on the Scholix Framework's "Information model" recommendations
 */

public enum ResourceType {
    /**
     * Indicates that the resource object is a dataset, and this can be one of the following types (for JDA collection)
     */
    COLLECTION,
    TEXT,
    SOFTWARE,
    OTHER,


    // Indicates that the resource object is publication.
    PUBLICATION,

    // The following are present in the ScholExplore collection
    LITERATURE,
    DATASET,
    UNKNOWN
}