package parsers;

import enums.RelationType;
import enums.ResourceType;

import lombok.Data;

import model.Identifier;
import model.ScholixLink;
import model.ScholixResource;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import utils.StringUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class implements the ResourceParser interface for (JSON parsing of) ScholeXplore collection.
 **/
@Data
public class ScholixLinkParser implements ResourceParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScholixLinkParser.class);

    // int numberOfLinks = 0; // Temporary control: Check the # of links harvested

    /** Default constructor */
    public ScholixLinkParser() { }

    /**
     * Given a link from ScholExplore collection (in JSON), this method parses it to a Link instance. The method returns
     * a Link instance.
     * @param sourceLink A Link instance in JSON
     * @return ScholixLink
     */
    @Override
    public ScholixLink parseLineToLink(String sourceLink) {
        // Set Scholix "Link info package" required elements
        ScholixLink aLink = null;
        LocalDate linkPublicationDate = LocalDate.now();
        List<String> linkProviders = new LinkedList<>();
        RelationType relationType = null;
        String licenseURL = null;

        ScholixResource source = new ScholixResource();
        ScholixResource target = new ScholixResource();
        List<String> authorList;
        List<String> publisherList;

        String tokenName; // Store current field name in the JSON document
        JsonFactory jsonFactory = new MappingJsonFactory();

        try (org.codehaus.jackson.JsonParser jsonParser = jsonFactory.createJsonParser(sourceLink)) {
            // Start parsing the JSON document, i.e. the "{"
            JsonToken currentToken = jsonParser.nextToken();
            if (currentToken != JsonToken.START_OBJECT) {
                System.out.println("Error: root should be object! Quitting...");
                return aLink;
            }

            // Loop over all the JSON tokens in the file
            while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
                tokenName = jsonParser.getCurrentName();

                // Process LinkPublicationDate: Mandatory element; ISO 8601 standard to be applied (Year-Month-Day)
                if (tokenName.equals("LinkPublicationDate")) {
                    jsonParser.nextToken(); // Get the value for said field name
                    String strLinkPublicationDate = jsonParser.getText();
                    linkPublicationDate = StringUtils.handleDateFormat(strLinkPublicationDate);
                }

                // Process LinkProvider: Mandatory element, 1..n
                if (tokenName.equals("LinkProvider")) {
                    // tokenName = jsonParser.getCurrentName();
                    // System.out.println("\nCurrent field: " + tokenName);
                    JsonToken arrayToken = jsonParser.nextToken(); // The "[" token
                    if (arrayToken == JsonToken.START_ARRAY) {
                        // Although an array, we only need the first JSON object for this element
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {
                            // Read record in a tree model
                            JsonNode linkProviderNode = jsonParser.readValueAsTree();

                            // Access fields of interest and add it to the Link instance
                            String linkProvider = linkProviderNode.get("name").getTextValue();
                            linkProviders.add(linkProvider);

                            // Add the link to a hash map: To get the # of link providers
                            // storeLinkProvider(linkProvider);

                            // Skip the rest of the JSON object, as it is unnecessary for our case.
                            jsonParser.skipChildren();
                        }

                        // Add link provider to the list: only store the first element of the (potential array)
                        // aLink.setProviders(linkProviders);
                    } else {
                        System.out.println("Error: records should be an array! Skipping ahead...");
                        jsonParser.skipChildren();
                    }
                }

                // Process RelationshipType: Mandatory element
                if (tokenName.equals("RelationshipType")) {
                    if (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                        JsonNode relationshipTypeNode = jsonParser.readValueAsTree();
                        String strRelationType = relationshipTypeNode.get("Name").getTextValue();
                        relationType = RelationType.valueOf(strRelationType);
                        jsonParser.skipChildren();
                    } else {
                        // System.out.println("Error: fields should be in a JSON object! Skipping..."); // Control message
                        jsonParser.skipChildren();
                    }
                }

                // Process LicenseURL: Optional element
                if (tokenName.equals("LicenseURL")) {
                    jsonParser.nextToken(); // Get the value for said field name
                    String tempLicenseURL = jsonParser.getText();

                    if(!tempLicenseURL.isEmpty()){
                        licenseURL = tempLicenseURL;
                    }
                }

                // Process "source" attributes
                if (tokenName.equals("Source")) {
                    if (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                        JsonNode sourceNode = jsonParser.readValueAsTree();

                        // Retrieve identifier: Mandatory element
                        String sourceID = sourceNode.get("Identifier").get("ID").getTextValue();
                        String sourceIdScheme = sourceNode.get("Identifier").get("IDScheme").getTextValue();

                        String sourceIdURL;
                        // The "IDURL" attribute is optional: check for its value (null) before assigning it.
                        JsonNode sourceIdUrlNode = sourceNode.get("Identifier").get("IDURL");
                        if(sourceIdUrlNode != null){
                            sourceIdURL = sourceIdUrlNode.getTextValue();
                        } else {
                            sourceIdURL = null;
                        }

                        source.setIdentifier(new Identifier(sourceID, sourceIdScheme, sourceIdURL));

                        // Type: Mandatory element
                        String sourceType = sourceNode.get("Type").get("Name").getTextValue();
                        source.setType(ResourceType.valueOf(sourceType.toUpperCase()));
                        // storeLinkResourceType(sourceType); Used for statistical purposes

                        // Title: Optional element
                        JsonNode sourceTitleNode = sourceNode.get("Title");
                        if (sourceTitleNode != null) {
                            String sourceTitle = sourceTitleNode.getTextValue();
                            if (!sourceTitle.isEmpty()) {
                                source.setTitle(sourceTitle);
                            }
                        }

                        // Creator(s): Optional element
                        authorList = new LinkedList<>();
                        JsonNode sourceCreatorsNode = sourceNode.get("Creator");
                        if((sourceCreatorsNode != null) && (sourceCreatorsNode.size() > 0)) {
                            for (Iterator<JsonNode> it = sourceCreatorsNode.getElements(); it.hasNext(); ) {
                                JsonNode creatorNameNode = it.next();
                                String creatorName = creatorNameNode.get("Name").getTextValue();
                                authorList.add(creatorName);
                            }

                            // Add all authors of this resource
                            source.setAuthors(authorList);
                        }

                        /* Publication date: Optional element
                            Note: Due to the variety of date representation, we could either store this value as a string,
                            or omit it altogether (since it is an optional element).
                        */
                        JsonNode sourcePublicationDateNode = sourceNode.get("PublicationDate");
                        if (sourcePublicationDateNode != null) {
                            String sourcePublicationDate = sourcePublicationDateNode.getTextValue();
                            if (!sourcePublicationDate.isEmpty()) {
                                source.setPublicationDate(StringUtils.handleDateFormat(sourcePublicationDate));
                            }
                        }

                        // Publisher(s): Optional element
                        publisherList = new LinkedList<>();
                        JsonNode sourcePublisherNode = sourceNode.get("Publisher");
                        if ((sourcePublisherNode != null) && (sourcePublisherNode.size() > 0)) {
                            for (Iterator<JsonNode> it = sourcePublisherNode.getElements(); it.hasNext(); ) {
                                JsonNode publisherNameNode = it.next();
                                String publisherName = publisherNameNode.get("name").getTextValue();
                                publisherList.add(publisherName);
                            }

                            // Add only 1 publisher (Scholix Framework cardinality for this element)
                            source.setPublisher(publisherList.get(0));
                        }
                    }
                }

                // Process "target"
                if (tokenName.equals("Target")) {
                    if (jsonParser.nextToken() == JsonToken.START_OBJECT) {
                        JsonNode targetNode = jsonParser.readValueAsTree();

                        // Retrieve identifier: Mandatory element
                        String targetID = targetNode.get("Identifier").get("ID").getTextValue();
                        String targetIdScheme = targetNode.get("Identifier").get("IDScheme").getTextValue();

                        String targetIdURL;
                        // The "IDURL" attribute is optional: check for its value (null) before assigning it.
                        JsonNode targetIdUrlNode = targetNode.get("Identifier").get("IDURL");
                        if(targetIdUrlNode != null){
                            targetIdURL = targetIdUrlNode.getTextValue();
                        } else {
                            targetIdURL = null;
                        }

                        target.setIdentifier(new Identifier(targetID, targetIdScheme, targetIdURL));

                        // Type: Mandatory element
                        String targetType = targetNode.get("Type").get("Name").getTextValue();
                        target.setType(ResourceType.valueOf(targetType.toUpperCase()));
                        // storeLinkResourceType(targetType); // Gathering statistics and features of the dataset.

                        // Title: Optional element
                        JsonNode targetTitleNode = targetNode.get("Title");
                        // String targetTitle = targetNode.get("Title").getTextValue();
                        if (targetTitleNode != null) {
                            String targetTitle = targetTitleNode.getTextValue();
                            if (!targetTitle.isEmpty()) {
                                target.setTitle(targetTitle);
                            }
                        }

                        // Creator(s): Optional element
                        authorList = new LinkedList<>();
                        JsonNode targetCreatorsNode = targetNode.get("Creator");
                        if ((targetCreatorsNode != null) && (targetCreatorsNode.size() > 0)) {
                            for (Iterator<JsonNode> it = targetCreatorsNode.getElements(); it.hasNext(); ) {
                                JsonNode creatorNameNode = it.next();
                                String creatorName = creatorNameNode.get("Name").getTextValue();
                                authorList.add(creatorName);
                            }

                            // Add all authors of this resource
                            target.setAuthors(authorList);
                        }

                        // Publication date: Optional element
                        JsonNode targetPublicationDateNode = targetNode.get("PublicationDate");
                        if(targetPublicationDateNode != null) {
                            String targetPublicationDate = targetPublicationDateNode.getTextValue();
                            if (!targetPublicationDate.isEmpty()) {
                                target.setPublicationDate(StringUtils.handleDateFormat(targetPublicationDate));
                            }
                        }

                        // Publisher(s): Optional element
                        publisherList = new LinkedList<>();
                        JsonNode targetPublisherNode = targetNode.get("Publisher");
                        if ((targetPublisherNode != null) && (targetPublisherNode.size() > 0)) {
                            for (Iterator<JsonNode> it = targetPublisherNode.getElements(); it.hasNext(); ) {
                                JsonNode publisherNameNode = it.next();
                                String publisherName = publisherNameNode.get("name").getTextValue();
                                publisherList.add(publisherName);
                            }

                            // Add only 1 publisher (Scholix Framework cardinality for this element)
                            target.setPublisher(publisherList.get(0));
                        }
                    }
                }
            }

            // Set all the Link instance attributes, now that the JSON link has been parsed
            aLink = new ScholixLink(linkPublicationDate, linkProviders, relationType, licenseURL, source, target);
        } catch (IOException jgm) {
            jgm.printStackTrace();
        }

        return aLink;
    }
}