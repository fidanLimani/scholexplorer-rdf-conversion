package parsers;

import model.AbstractLink;

/**
 * This interface represents the behavior that a parser for any type of resource (API, JSON, XML, etc.), or any
 * collection (ScholExplore, EconBiz-JDA, ResearchObject, etc.) should provide.

ScholExplore collection
    - void parseScholExploreFile(String fileName)
        Read file content line by line and pass each separate line to parse it as separate links

    - Link parseScholExploreLink(String jsonLinks)
        This method parses the ScholExplore JSON collection file. Potential parameters include "data providers". The
        method should return a single Link instance; the List<Link> will add these individual instances in method
        parseScholEploreFile(...).

EconBiz - JDA links
 List<String> parseDatasetEntries(String contents)
    - This method returns JDA data collection entries, taking a REST response as an input.
 void parseJDAEntry(String datasetContent)
    - This method returns the details of a single dataset entry from the JDA collection
 List<Resource> parseEBEntry(String datasetResult)
    - This method returns the details of a matching (if exists) dataset from a list of candidate EconBiz results
 **/
public interface ResourceParser {
    /** The link you read from the file is parsed to create a Link instance. **/
    AbstractLink parseLineToLink(String sourceLink);
}
