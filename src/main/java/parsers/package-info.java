/**
 * This package contains the parser implementations for the specific sources, starting with ScholExplore. Other to be
 * included in the future include: EconBiz - JDA, ResearchObject, RMap, etc.
 **/

package parsers;