package model;

import constants.ModelConstants;
import enums.RelationType;
import lombok.Data;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

/** This class models the publication-to-data link, based on the Scholix Framework's "Information model" */
@Data
public class Link {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Link.class);

    private Resource sourceModel; // Dataset -> Source (JDA)
    private Resource targetModel; // Publication -> Target (EconBiz)

    // Data-to-Publication linking: relationType="isCitedBy"; inverseRelationType="cites";
    private RelationType relationType;

    // Removed from Scholix "Link information package", but we keep it to offer bidirectional linking of resources.
    private RelationType inverseRelationType;

    /* The following properties are pretty "static" in nature, as we are using links that related resources within the
    same institution (ZBW) */
    private LocalDate publicationDate; // the date the links are established
    private String linkProvider; // the link provider; for JDA - EconBiz it is "ZBW".

    // License URL, for e.g.: "LicenseURL": "http://creativecommons.org/publicdomain/zero/1.0"
    private String licenseURL;

    // Default constructor
    public Link(){
        this.sourceModel = new Resource();
        this.targetModel = new Resource();
        publicationDate = LocalDate.now();
    }

    //The link's direction is from dataset to publication; that's how we assign relationship types to a Link instance.
    public Link(Resource sourceModel, Resource targetModel){
        this.sourceModel = Objects.requireNonNull(sourceModel, "Link source cannot be null");
        this.targetModel = Objects.requireNonNull(targetModel, "Link target cannot be null");
        this.publicationDate = LocalDate.now();
    }

    // A method to create a test Link, including source and target Resource instances
    public static Link generateLink(){
        Link aLink = new Link();
        Resource sourceModel = new Resource();
        Resource targetModel = new Resource();

        // Test handling non-available dates in the app
        LocalDate myDate = LocalDate.now();

        // Initialize the source model to test DB operations
        LOGGER.info("Initializing 'source' and 'target' Model instances...");
        sourceModel.setPublicationDate(LocalDate.parse("2018-01-12"));
        sourceModel.setAuthors(Arrays.asList("Jane Doe", "John Smith", "ZBW"));
        sourceModel.setPublishers(Arrays.asList("IFW Magazine", "The Wall Street Journal"));
        sourceModel.setTitle("Outsourcing effects on the GDP of developing countries");
        sourceModel.setIdentifier("10011677788");
        sourceModel.setSubjectTerms(Arrays.asList(new Tag("GDP"), new Tag("Outsourcing")));
        sourceModel.setResourcePortal("JDA");
        sourceModel.setResourceType("Dataset");
        sourceModel.setNumberOfResources(3);
        // sourceModel.setLicenceURL();

        // Initialize the target model to test DB operations
        targetModel.setPublicationDate(LocalDate.parse("2017-03-22"));
        targetModel.setAuthors(Arrays.asList("John", "Doe", "JDA"));
        // targetModel.setPublishers(Arrays.asList("World Economic Magazine"));
        targetModel.setPublishers(Collections.singletonList("World Economic Magazine"));
        targetModel.setTitle("Climate change effects on the fishery industry of Northern Germany");
        targetModel.setIdentifier("10011677788");
        targetModel.setSubjectTerms(Arrays.asList(new Tag("Fisheries"), new Tag("Climate change"),
                new Tag("Income")));
        targetModel.setResourcePortal("EconBiz");
        targetModel.setResourceType("Publication");
        targetModel.setNumberOfResources(0); // This value would be initialized to 0 by default. #unnecessary

        LOGGER.info("Set Link instance attributes...");
        aLink.setSourceModel(sourceModel);
        aLink.setTargetModel(targetModel);
        aLink.setPublicationDate(myDate); // Could this value be null?
        aLink.setLinkProvider("ScholExplorer");
        aLink.setRelationType(RelationType.IsCitedAsDataSourceBy);
        aLink.setInverseRelationType(RelationType.CitesAsDataSource); // I.e., the triple
                                                                      // (publication, cite:citesAsDataSource, dataset)
        aLink.setLicenseURL(ModelConstants.LINK_LICENSE);

        return aLink;
    }

    // Override toString()
    @Override
    public String toString(){
        return "Link\n" +
                "\tSource title=" + getSourceModel().getTitle() + " (" + getSourceModel().getResourceType() + ")\n" +
                "\tRelationship type=" + getRelationType() + '\n' +
                "\tTarget title=" + getTargetModel().getTitle() + " (" + getTargetModel().getResourceType() + ")\n" +
                "\tInverse relationship type=" + getInverseRelationType() + '\n' +
                "\tLink licence=" + getLicenseURL() + '\n' +
                "\tLink publication date=" + getPublicationDate();
    }

    // Test the app
    public static void main(String[] args){
        Link testLink = Link.generateLink();
        System.out.println("Demo Link object: \n" + testLink);
    }
}