package model;

import lombok.Data;

/**
 * This class models the author details: first name, last name, and affiliation
 */
@Data
public class Author {
    private String firstName;
    private String lastName;
    private String affiliation;
}