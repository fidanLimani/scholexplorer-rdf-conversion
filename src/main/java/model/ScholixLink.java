package model;

import enums.RelationType;
import enums.ResourceType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ScholixLink extends AbstractLink
{
    private String licenseURL;

    /* Removed from Scholix "Link information package", but we keep it to offer bidirectional linking of resources. */
    // private RelationType inverseRelationType;

    private AbstractResource sourceObject;
    private AbstractResource targetObject;

    /** All-args constructor: Check that mandatory elements are passed in! **/
    public ScholixLink(LocalDate publicationDate, List<String> providers, RelationType relationType, String licenseURL,
                       AbstractResource sourceObject, AbstractResource targetObject) {
        super(publicationDate, providers, relationType);
        this.licenseURL = licenseURL;
        this.sourceObject = Objects.requireNonNull(sourceObject, "Link source cannot be null");
        this.targetObject = Objects.requireNonNull(targetObject, "Link target cannot be null");
    }

    public void setLicenseURL(String licenseURL) {
        this.licenseURL = licenseURL;
    }

    public void setSourceObject(AbstractResource sourceObject) {
        this.sourceObject = sourceObject;
    }

    public void setTargetObject(AbstractResource targetObject) {
        this.targetObject = targetObject;
    }

    public String getLicenseURL() {
        return licenseURL;
    }

    public AbstractResource getSourceObject() {
        return sourceObject;
    }

    @Override
    public String toString() {
        return "ScholixLink{" +
                "licenseURL='" + licenseURL + '\'' +
                ", sourceObject=" + sourceObject +
                ", targetObject=" + targetObject +
                ", publicationDate=" + publicationDate +
                ", providers=" + providers +
                ", relationType=" + relationType +
                '}';
    }

    public AbstractResource getTargetObject() {
        return targetObject;
    }

    // A method to create a test Link instance
    public static AbstractLink generateTestLink(){
        AbstractLink aLink;
        AbstractResource sourceObject = new ScholixResource();
        AbstractResource targetObject = new ScholixResource();

        // Link attributes: Publication date & Relationship type will be provided at method invocation
        List<String> linkProviders = List.of("ScholExplorer", "Econ Biz");
        String myLicenseURL = "https://creativecommons.org/licenses/by/4.0/";

        // Initialize the source object
        sourceObject.setPublicationDate(LocalDate.parse("2018-11-12"));
        sourceObject.setAuthors(Arrays.asList("Jane Doe", "John Smith", "Karl Jung"));
        sourceObject.setPublisher("The Wall Street Journal");
        sourceObject.setTitle("Outsourcing effects on the GDP of developing countries");
        sourceObject.setIdentifier(new Identifier("10011677788", "Handle", "http://example.org"));
        sourceObject.setType(ResourceType.DATASET);

        // Initialize the target model to test DB operations
        targetObject.setPublicationDate(LocalDate.parse("2017-03-20"));
        targetObject.setAuthors(Arrays.asList("Jimmy Fallon", "Anna Kendrick"));
        targetObject.setPublisher("World Economic Magazine");
        targetObject.setTitle("Climate change effects on the fishery industry of Northern Germany");
        targetObject.setIdentifier(new Identifier("10011677700", "DOI", "http://1001234/test"));
        targetObject.setType(ResourceType.LITERATURE);

        aLink = new ScholixLink(LocalDate.now(), linkProviders, RelationType.References, myLicenseURL, sourceObject, targetObject);

        // System.out.println("A test link: \n" + aLink.toString());

        return aLink;
    }
}