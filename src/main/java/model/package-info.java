/*
  This package contains classes that model the link between scholarly resources - publications and datasets.
**/
package model;