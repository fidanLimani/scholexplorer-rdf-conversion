package model;

/** This class contains the author details: first name, last name, and affiliation */
public class Publisher {
    private String publisherName;

    public Publisher(){}

    public Publisher(String publisherName) {
        this.publisherName = publisherName;
    }

    // Getters
    public String getPublisherName() {
        return publisherName;
    }

    // Setters
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    @Override
    public String toString() {
        return "Publisher{" +
                "name='" + publisherName + '\'' +
                '}';
    }
}