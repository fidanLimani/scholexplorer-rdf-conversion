package model;

import lombok.Data;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This class represents the model that will support JDA - EconBiz search, and constitutes of metadata common to API
 * responses from both collections. We represent the parsed resource entry (EconBiz or JDA) via this class.
 */
@Data
public class Resource
{
    // Link Information Package: Mandatory elements
    private String identifier;
    private String resourceType;

    // Link Information Package: Optional elements
    private String title;

    /* private List<Publisher> publishers; */
    private List<String> publishers;

    /* We keep a simplified approach for now, as there are different attributes for authors in JDA, EconBiz, and
        ScholExplore. In the future, when a possible generalization would be required, we will reconsider re-using this
         class, instead of a "reduced" author version represented as a string. */
    // private List<Author> authors;
    private List<String> authors; // In the [LastName FirstName] pattern
    private LocalDate publicationDate;
    // private List<Identifier> identifiers;

    // "Extension": adding portal, subject terms, and number of resources as metadata present with JDA and EconBiz
    private String resourcePortal; // JDA, EconBiz, or ScholExplorer
    private List<Tag> subjectTerms;
    private long numberOfResources;

    // "Operational" metadata: used during harvesting of JDA-EconBiz links
    private boolean status; // Is a model complete, or not (missing results from EconBiz API)
    private String name; // Resource name from JDA entry, needed to construct a REST call to EconBiz
    // private String accessURL; // Could be nice to provide a URL of a resource for further access, if the user wants

    /* A default constructor */
    public Resource(){
        this.status = false;
    }

    /*
     * Constructor: Once information are extracted, instantiate a Model object to contain it
     * To-do: Change resourceType parameter from String to ObjectType enumeration!
     */
    public Resource(String resourcePortal, String title, List<String> authors, LocalDate publicationDate, String identifier,
                    List<String> publishers, List<Tag> subjectTerms, String resourceType, long numberOfResources) {
        this.resourcePortal = resourcePortal;
        this.title = title;
        this.authors = authors;
        this.publicationDate = publicationDate;
        this.identifier = identifier;
        this.publishers = publishers;
        this.subjectTerms = subjectTerms;
        this.resourceType = resourceType;
        this.numberOfResources = numberOfResources;
    }

    // Override toString()
    @Override
    public String toString(){
        return "Resource\n" +
                "\tresourcePortal=" + getResourcePortal() + '\n' +
                "\tresource type=" + getResourceType() + '\n' +
                "\ttitle=" + getTitle() + '\n' +
                "\tpublication date=" + getPublicationDate();
    }

    // Test the app
    public static void main(String[] args) {
        Resource model = new Resource();
        // Test handling non-available dates in the app
        // LocalDate myDate = LocalDate.now();

        // Initialize the source model to test DB operations
        System.out.println("Initializing 'source' and 'target' Model instances...");
        model.setPublicationDate(LocalDate.parse("2018-01-12"));
        model.setAuthors(Arrays.asList("Jane", "Doe", "ZBW"));
        // model.setPublishers(Arrays.asList("IFW Magazine"));
        model.setPublishers(Collections.singletonList("IFW Magazine"));
        model.setTitle("Outsourcing effects on the GDP of developing countries");
        model.setIdentifier("10011677788");
        model.setSubjectTerms(Arrays.asList(new Tag("GDP"), new Tag("Outsourcing")));
        model.setResourcePortal("JDA");
        model.setResourceType("Dataset");
        model.setNumberOfResources(3);

        System.out.println("Model output... \n" + model);
    }
}