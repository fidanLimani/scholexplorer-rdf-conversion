package model;

/**
 * This class models the tags used in JDA resources
 */
public class Tag {
    private String displayName;

    public Tag(){ }

    public Tag(String displayName){
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString(){
        return getDisplayName();
    }
}