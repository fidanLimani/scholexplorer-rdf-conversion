package model;

import enums.ResourceType;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class ScholixResource extends AbstractResource {
    /**
     * A default constructor
     **/
    public ScholixResource(){
        super();
    }

    /** All-args constructor */
    public ScholixResource(Identifier identifier, ResourceType type, String title, String publisher,
                           List<String> authors, LocalDate publicationDate) {
        this.identifier = Objects.requireNonNull(identifier, "Resource IDer cannot be null");
        this.type = Objects.requireNonNull(type, "Resource type cannot be null");
        this.title = title;
        this.publisher = publisher;
        this.authors = authors;
        this.publicationDate = publicationDate;
    }

    @Override
    public String toString() {
        return "ScholixResource{" +
                "identifier=" + identifier +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", publisher='" + publisher + '\'' +
                ", authors=" + authors +
                ", publicationDate=" + publicationDate +
                '}';
    }
}