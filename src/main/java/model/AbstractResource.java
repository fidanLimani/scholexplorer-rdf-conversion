package model;

import enums.ResourceType;

import java.time.LocalDate;
import java.util.List;

/**
 * What are the resources being linked or packaged together?
 **/
public class AbstractResource {
    Identifier identifier;
    ResourceType type;
    String title;
    String publisher; // Currently, we can leave it as a String instead of a more specific class, such as Publisher, for e.g.;
    List<String> authors;
    LocalDate publicationDate; // Publication or creation data, all the same

    /** Getter and Setter methods */
    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Identifier getIdentifier() {
        return identifier;
    }

    public ResourceType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }
}