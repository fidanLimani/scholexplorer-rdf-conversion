package model;

/**
 * This class contains the Identifier details: ID, ID Scheme, and ID URL
 */
public class Identifier {
    private String id;
    private String idScheme;
    private String idURL;

    public Identifier(){
        id = "";
        idScheme = "";
        idURL = "";
    }

    public Identifier(String id, String idScheme, String idURL){
        this.id = id;
        this.idScheme = idScheme;
        this.idURL = idURL;
    }

    public String getId() {
        return id;
    }

    public String getIdScheme() {
        return idScheme;
    }

    public String getIdURL() {
        return idURL;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdScheme(String idScheme) {
        this.idScheme = idScheme;
    }

    public void setIdURL(String idURL) {
        this.idURL = idURL;
    }

    @Override
    public String toString() {
        return "Identifier{" +
                "id='" + id + '\'' +
                ", idScheme='" + idScheme + '\'' +
                ", idURL='" + idURL + '\'' +
                '}';
    }
}
