package model;

import enums.RelationType;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/** As we add more "links" formats (besides Scholix Framework), we will add common fields and methods to this class. **/
public abstract class AbstractLink
{
    protected LocalDate publicationDate;
    protected List<String> providers;
    protected RelationType relationType;

    /** Default constructor: We need it to set up Link instance attributes as we parse a JSON  */
    public AbstractLink () {}

    /** All-args constructor */
    public AbstractLink(LocalDate publicationDate, List<String> providers, RelationType relationType){
        this.publicationDate = Objects.requireNonNull(publicationDate, "Link publication date cannot be null!");
        this.providers = Objects.requireNonNull(providers, "Link provider(s) cannot be null!");
        this.relationType = Objects.requireNonNull(relationType, "Link relationship type cannot be null!");
    }

    /** Getter and Setter methods */
    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setProviders(List<String> providers) {
        this.providers = providers;
    }

    public void setRelationType(RelationType relationType) {
        this.relationType = relationType;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public List<String> getProviders() {
        return providers;
    }

    public RelationType getRelationType() {
        return relationType;
    }
}