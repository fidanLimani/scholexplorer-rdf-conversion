package harvest;

import java.nio.file.Path;

public interface LinkHarvester {
    void harvestLinkCollectionFromDir(Path directoryPath);

    /*
     * Other methods that parse from API, or other sources, for e.g.:
     * public void harvestLinkCollectionFromAPI(String apiURI);
     *
     * etc.
     */
}