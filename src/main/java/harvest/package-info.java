/** This package contains 2 types of classes:
 * (1) Harvest entries from JDA and EconBiz
 * (2) Harvest ScholExplore collection and convert its items to Link instances
 **/
package harvest;