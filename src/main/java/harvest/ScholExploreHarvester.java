package harvest;

import db.RDFStore;
import enums.ResourceType;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import model.ScholixLink;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import parsers.ScholixLinkParser;
import rdf_conversion.LinkRdfConverter;
import utils.FileIO;

import java.io.*;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;

import static constants.DBConstants.SCHOL_LINK_DATASET;
import static constants.HarvestLogging.*;
import static constants.RDFConversion.GRAPH_URI;
import static constants.RDFConversion.PROV_GRAPH_NAME;

/*
    This class harvests the ScholeXplorer collection
**/
public class ScholExploreHarvester implements LinkHarvester {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScholExploreHarvester.class);
    private ScholixLinkParser scholixLinkParser;
    private ProgressBarBuilder progressBarBuilder;

    // Setup the class that converts a Link instance to RDF and stores it to Jena TDB
    LinkRdfConverter linkRdfConverter;
    private RDFStore rdfStore;

    // Specify the provenance of the link conversion to RDF
    private Instant startedAt, endedAt;

    /**
     * Default constructor
     */
    public ScholExploreHarvester() {
        scholixLinkParser = new ScholixLinkParser();
        rdfStore = new RDFStore(SCHOL_LINK_DATASET);
        linkRdfConverter = new LinkRdfConverter();

        // Setup the ProgressBars
        progressBarBuilder = new ProgressBarBuilder()
                .setTaskName("Reading")
                .setUnit("MB", 1048576)
                .setStyle(ProgressBarStyle.COLORFUL_UNICODE_BLOCK);
    }

    /**
     * Harvest and convert links from files in a directory
     * @param directoryPath The name of the directory to parse
     */
    public void harvestLinkCollectionFromDir(Path directoryPath) {
        File folder = new File(directoryPath.toString());
        File[] scholExploreFiles = folder.listFiles();

        if (scholExploreFiles != null) {
            startedAt = Instant.now();

            for (File file : scholExploreFiles) {
                try {
                    LOGGER.info("Parsing a resource collection from file: " + file.getName());
                    harvestFromFile(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Once all the ScholExplorer files are converted, add the provenance as a final (RDF) model
            endedAt = Instant.now();
            Duration timeElapsed = Duration.between(startedAt, endedAt);

            // Model provenanceModel = linkRdfConverter.createProvenanceModel(startedAt, endedAt);
            Model provenanceModel = linkRdfConverter.createProvenanceModel();
            rdfStore.storeModelToTDB(String.format(GRAPH_URI, PROV_GRAPH_NAME), provenanceModel);

            // Back up the contents of Jena TDB: Add a new line for clarity of the logs
            LOGGER.info("Backing up and compressing the provenance information from the TDB...");
            rdfStore.backupAndCompressDataset();

            /* Optional
            System.out.println("Empty Jena TDB after backing its provenance information.");
            rdfStore.removeNamedGraph(String.format(GRAPH_URI, PROV_GRAPH_NAME));
             */

            LOGGER.info("Deleting the TDB records ...\n");
            FileIO.deleteDirectories(rdfStore.getLocation().getDirectoryPath());
            System.out.printf("\nHarvesting lasted for %d minutes.", timeElapsed.toMinutes());

            /*
            // Store statistics at the level of the whole collection
            rdfStore.generateDatasetStats(new File(String.format(SCHOL_EXPLORER_OUTPUT, SPARQL_OUT_FILE)));
            // Provide harvesting statistics, if required
            LOGGER.info("Harvest statistics...\n");
            displayStatistics();
             */
        }
    }

    /**
     * Read file content line by line and pass each separate line to parse it as separate links. Since the parsing takes
     * time, add progress bar functionality to this method.
     * @param file The name of the file to parse for its containing links in JSON
     */
    public void harvestFromFile(File file) {
        // Read individual JSON entries from the file
        try (BufferedReader br = new BufferedReader(new InputStreamReader(ProgressBar.wrap(
                new FileInputStream(file.getPath()), progressBarBuilder)))) {
            String sourceLink;
            AtomicInteger linkCounter = new AtomicInteger();
            Model modelBatch = ModelFactory.createDefaultModel();

            while ((sourceLink = br.readLine()) != null) {
                linkCounter.getAndIncrement();
                ScholixLink linkInstance = scholixLinkParser.parseLineToLink(sourceLink);
                modelBatch.add(linkRdfConverter.convertLink(linkInstance));

                // If the model batch has reached <linkCounter> entries, store it
                if (linkCounter.get() % LINK_CONVERSION_BATCH == 0) {
                    // Store Model instances to a named graph in Jena TDB #option1
                    // rdfStore.storeModelToTDB(String.format(GRAPH_URI, file.getName()), modelBatch);

                    // Store Model instances to a file #option2
                    // rdfStore.storeModelToFile(file.getName(), modelBatch, RDFFormat.NQUADS);

                    // Store the Model instances to the default graph to keep HDD space usage low #option3
                    rdfStore.storeLinkInstanceInDefaultGraph(modelBatch);

                    modelBatch = ModelFactory.createDefaultModel();
                    linkCounter.set(0);
                }
            }

            // In this part add all the remaining Link instances that did not "make" a batch
            System.out.printf("\nStoring the %d remaining items after the last batch: \n", linkCounter.get());
            // Store Model instances to a named graph in Jena TDB #option1
            // rdfStore.storeModelToTDB(String.format(GRAPH_URI, file.getName()), modelBatch);

            // Storing to a file #option2
            // rdfStore.storeModelToFile(file.getName(), modelBatch, RDFFormat.NQUADS);

            // Store the Model instances to the default graph to keep HDD space usage low #option3
            rdfStore.storeLinkInstanceInDefaultGraph(modelBatch);

            // Back up the contents of Jena TDB, then delete its contents to free up space
            LOGGER.info("Backing up and compressing the contents of the TDB...");
            rdfStore.backupAndCompressDataset();

            // Optionally delete triples on a named graph
            LOGGER.info("Empty Jena TDB named graph after backing its content up.");
            rdfStore.removeNamedGraph(String.format(GRAPH_URI, file.getName()));

            /*
            LOGGER.info("Deleting the TDB records ...");
            FileIO.deleteDirectories(rdfStore.getLocation().getDirectoryPath());
             */
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * Harvest and store dataset titles to an spreadsheet
     * @param directoryPath The name of the directory to parse
     */
    public void harvestLinkCollectionFromDirToFile(Path directoryPath) {
        File folder = new File(directoryPath.toString());
        File[] scholExploreFiles = folder.listFiles();

        if (scholExploreFiles != null) {
            startedAt = Instant.now();

            for (File file : scholExploreFiles) {
                try {
                    LOGGER.info("Parsing a resource collection from file: " + file.getName());
                    harvestFromFileToFile(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            endedAt = Instant.now();
            Duration timeElapsed = Duration.between(startedAt, endedAt);
            System.out.printf("Harvesting lasted for %d minutes.\n", timeElapsed.toMinutes());
        }
    }

    /**
     * Read file content line by line and pass each separate line to parse it as separate links. Since the parsing takes
     * time, add progress bar functionality to this method.
     *
     * @param file The name of the file to parse for its containing links in JSON
     */
    public void harvestFromFileToFile(File file) {
        // Read individual JSON entries from the file
        File fileName = new File("src/main/resources/output/scholexplorerDatasetTitles.txt");
        int rowCount = 0;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(ProgressBar.wrap(
                new FileInputStream(file.getPath()), progressBarBuilder)))) {
            String sourceLink;
            AtomicInteger linkCounter = new AtomicInteger();
            String sourceTitle, targetTitle;

            while ((sourceLink = br.readLine()) != null) {
                linkCounter.getAndIncrement();
                ScholixLink linkInstance = scholixLinkParser.parseLineToLink(sourceLink);

                // Filter datasets only
                if(linkInstance.getSourceObject().getType().equals(ResourceType.DATASET)) {
                    sourceTitle = linkInstance.getSourceObject().getTitle();
                    FileIO.appendToFile(sourceTitle, fileName);
                }

                if(linkInstance.getTargetObject().getType().equals(ResourceType.DATASET)) {
                    targetTitle = linkInstance.getTargetObject().getTitle();
                    FileIO.appendToFile(targetTitle, fileName);
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}