package harvest;

import lombok.Data;
import model.AbstractLink;
import model.ScholixLink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provides methods to process (or calculate) different aspects of the harvested collection, such as types of
 * artifacts linked, number of providers, publication ranges, and other statistics that are important to describe the
 * collection, given any file from the ScholExplore collection.
 */
@Data
public class LinkProcessor {
    private Map<String, Integer> linksPerFile;
    private Map<String, Integer> linksPerProviderMap;
    private Map<String, Integer> objectTypeMap;

    // What relation types are present in ScholExplore collection
    private Map<String, Integer> relationTypes;

    /** A default constructor */
    public LinkProcessor() {
        linksPerFile = new HashMap<>();
        linksPerProviderMap = new HashMap<>();
        relationTypes = new HashMap<>();
        objectTypeMap = new HashMap<>();
    }

    /** A method that invokes all the statistics-based methods from this class */
    public void collectLinkStatistics() {

    }

    /*
     * Store object types (source and target) of the collection
     * @parm linkInstance A Link instance
     */
    public void calculateObjectType(AbstractLink linkInstance) {
        List<String> objectTypes = new ArrayList<>();
        objectTypes.add(((ScholixLink) linkInstance).getSourceObject().getType().name());
        objectTypes.add(((ScholixLink) linkInstance).getTargetObject().getType().name());
        for (String resourceType : objectTypes) {
            if (objectTypeMap.containsKey(resourceType)) {
                objectTypeMap.put(resourceType, objectTypeMap.get(resourceType) + 1);
            } else {
                objectTypeMap.put(resourceType, 1);
            }
        }
    }

    /*
     * Store links per provider
     * @parm linkInstance A Link instance parsed from a file
     * @note While parsing ScholExplore link collection, we get the following:
            ScholixLink scholixLink = parseLineToLink(tempData);
      In this part, we can invoke any of the LinkProcessor methods.
     */
    public void calculateLinksPerProvider(AbstractLink linkInstance) {
        // Add the link count for all [1..N] providers of a link instance
        List<String> linkProviders;
        linkProviders = linkInstance.getProviders();
        for (String linkProvider : linkProviders) {
            if (linksPerProviderMap.containsKey(linkProvider)) {
                linksPerProviderMap.put(linkProvider, linksPerProviderMap.get(linkProvider) + 1);
            } else {
                linksPerProviderMap.put(linkProvider, 1);
            }
        }
    }

    /*
    * Store links per provider
    * @parm linkInstance A Link instance parsed from a file
    * @note While parsing ScholExplore link collection, we get the following:
           ScholixLink scholixLink = parseLineToLink(tempData);
     In this part, we can invoke any of the LinkProcessor methods.
    */
    public void calculateRelations(AbstractLink linkInstance) {
        String relationType = linkInstance.getRelationType().name();
        if (relationTypes.containsKey(relationType)) {
            relationTypes.put(relationType, relationTypes.get(relationType) + 1);
        } else {
            relationTypes.put(relationType, 1);
        }
    }

    /**
     * Display map contents
     */
    public void displayMapContent(Map<String, Integer> aMap) {
        for (Map.Entry<String, Integer> e : aMap.entrySet()) {
            System.out.println(e.getKey() + ": " + e.getValue());
        }
    }
}