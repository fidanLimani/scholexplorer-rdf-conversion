package constants;

import java.util.AbstractMap;
import java.util.Map;

/**
 * This static class contains constants that are used to log JDA's RESTful service usage.
 * @author Fidan Limani
 */
public class HarvestLogging {
    /* The original ScholExplore data dump, the processed (more small files) version, and the output of te result
    (RDF and SPARQL results in a file) */
    public static final String SCHOL_EXPLORER_INPUT = "src/main/resources/input";
    public static final String SCHOL_EXPLORER_PROCESSED = "src/main/resources/processed/%s/";
    public static final String SCHOL_EXPLORER_OUTPUT = "src/main/resources/output/%s";
    public static final String SPARQL_OUT_FILE = "sparqlOutputFile.txt";

    /* During harvest, store parsed Model instances in a List until they reach a considerable # before storing them to
       Jena TDB. This is meant to keep the DB-related I/O as small as possible. */
    public static final int LINK_CONVERSION_BATCH = 5000;

    // Splitting the original Scholexplorer files to smaller units: How many links should a file contain? 1 M links?
    public static final int LINK_FILE_BATCH = 300000;

    // Links per file (Used to track the progress of the whole process)
    public static final Map<String, Integer> linksPerFile = Map.ofEntries(
            new AbstractMap.SimpleEntry<>("part-r-00000", 4817513),
            new AbstractMap.SimpleEntry<>("part-r-00001", 4155104),
            new AbstractMap.SimpleEntry<>("part-r-00002", 3892320),
            new AbstractMap.SimpleEntry<>("part-r-00003", 4019984),
            new AbstractMap.SimpleEntry<>("part-r-00004", 4555259),
            new AbstractMap.SimpleEntry<>("part-r-00005", 4009990),
            new AbstractMap.SimpleEntry<>("part-r-00006", 4272618),
            new AbstractMap.SimpleEntry<>("part-r-00007", 4153642),
            new AbstractMap.SimpleEntry<>("part-r-00008", 4061450),
            new AbstractMap.SimpleEntry<>("part-r-00009", 4303453),
            new AbstractMap.SimpleEntry<>("part-r-00010", 3918854),
            new AbstractMap.SimpleEntry<>("part-r-00011", 4118775),
            new AbstractMap.SimpleEntry<>("part-r-00012", 4269474),
            new AbstractMap.SimpleEntry<>("part-r-00013", 4241714),
            new AbstractMap.SimpleEntry<>("part-r-00014", 4256970),
            new AbstractMap.SimpleEntry<>("part-r-00015", 3877543),
            new AbstractMap.SimpleEntry<>("part-r-00016", 4521111),
            new AbstractMap.SimpleEntry<>("part-r-00017", 4183914),
            new AbstractMap.SimpleEntry<>("part-r-00018", 4324506),
            new AbstractMap.SimpleEntry<>("part-r-00019", 4192615),
            new AbstractMap.SimpleEntry<>("part-r-00020", 4369380),
            new AbstractMap.SimpleEntry<>("part-r-00021", 3985198),
            new AbstractMap.SimpleEntry<>("part-r-00022", 4665523),
            new AbstractMap.SimpleEntry<>("part-r-00023", 4345333),
            new AbstractMap.SimpleEntry<>("part-r-00024", 4879491),
            new AbstractMap.SimpleEntry<>("part-r-00025", 3928615),
            new AbstractMap.SimpleEntry<>("part-r-00026", 3878106),
            new AbstractMap.SimpleEntry<>("part-r-00027", 4291171),
            new AbstractMap.SimpleEntry<>("part-r-00028", 4083575),
            new AbstractMap.SimpleEntry<>("part-r-00029", 4188986));
}