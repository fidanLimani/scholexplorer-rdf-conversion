package constants;

// THis class contains the constants required for RDF modeling, and this mainly includes the namespace-related info.
public class RDFConversion
{
    // Namespace definitions for the model
    public static final String BASE_URI = "http://zbw.eu/scholarly-resources/%s/";

    // Namespace prefixes specific to the Scholarly Graph
    public static final String GRAPH_PREFIX = "graph/%s";
    public static final String GRAPH_URI = String.format(BASE_URI, GRAPH_PREFIX);

    public static final String LINK_PREFIX = "link";
    public static final String LINK = String.format(BASE_URI, LINK_PREFIX);

    public static final String PUBLICATION_PREFIX = "publication";
    public static final String PUBLICATION_URI = String.format(BASE_URI, PUBLICATION_PREFIX);

    public static final String DATASET_PREFIX = "dataset";
    public static final String DATASET_URI = String.format(BASE_URI, DATASET_PREFIX);

    // ScholExplorer collection contains resources of type publication, dataset, and unknown;
    public static final String UNKNOWN_PREFIX = "unknown";
    public static final String UNKNOWN_URI = String.format(BASE_URI, UNKNOWN_PREFIX);

    public static final String LINK_PROVIDER_PREFIX = "provider";
    public static final String LINK_PROVIDER = String.format(BASE_URI, LINK_PROVIDER_PREFIX);

    // Used to create (primary || alternate) identifier instances
    public static final String IDENTIFIER_PREFIX = "identifier";
    public static final String IDENTIFIER = String.format(BASE_URI, IDENTIFIER_PREFIX);

    // Used to create (primary || alternate) identifier instances
    public static final String IDENTIFIER_SCHEME_PREFIX = "scheme";
    public static final String IDENTIFIER_SCHEME = String.format(BASE_URI, IDENTIFIER_SCHEME_PREFIX);

    // Provenance
    public static final String PROVENANCE_PREFIX = "provenance";
    public static final String PROVENANCE_URI = String.format(BASE_URI, PROVENANCE_PREFIX);

    // Used to create Portals
    public static final String PORTAL_PREFIX = "portal";
    public static final String PORTAL = String.format(BASE_URI, PORTAL_PREFIX);

    // Used to create (STW) concepts
    public static final String STW_CONCEPT_PREFIX = "stw-concept";
    public static final String STW_CONCEPT = String.format(BASE_URI, STW_CONCEPT_PREFIX);

    // Used to create dataset size for the different instances
    public static final String DS_SIZE_PREFIX = "ds-measure";
    public static final String DS_SIZE = String.format(BASE_URI, DS_SIZE_PREFIX);

    // Namespace prefixes for the ontologies and vocabularies uesd to model Scholix Framework links
    public static final String DATA_CITE_PREFIX = "datacite";
    public static final String DATA_CITE = "http://purl.org/spar/datacite/";

    public static final String CITO_PREFIX = "cito";
    public static final String CITO = "http://purl.org/spar/cito/";

    public static final String DC_TERMS_PREFIX = "dcterms";
    public static final String DC_TERMS = "http://purl.org/dc/terms/";

    public static final String FOAF_PREFIX = "foaf";
    public static final String FOAF = "http://xmlns.com/foaf/0.1/";

    public static final String RDF_PREFIX = "rdf";
    // Having it as "RDF" conflicts with the org.apache.jena.vocabulary.RDF class, hence the "URI" addition;
    public static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

    public static final String RDFS_PREFIX = "rdfs";
    public static final String RDFS_URI = "http://www.w3.org/2000/01/rdf-schema#";

    public static final String XSD_PREFIX = "xsd";
    public static final String XSD = "http://www.w3.org/2001/XMLSchema#";

    public static final String PROV_PREFIX = "prov";
    public static final String PROV = "http://www.w3.org/ns/prov#";

    public static final String BIB_PREFIX = "bib";
    public static final String BIB = "http://id.loc.gov/ontologies/bibframe/";

    public static final String CC_PREFIX = "cc";
    public static final String CC = "http://creativecommons.org/ns#";

    public static final String EDM_PREFIX = "edm";
    public static final String EDM = "http://www.europeana.eu/schemas/edm/";

    public static final String LITERAL_PREFIX = "literal";
    public static final String LITERAL = "http://www.essepuntato.it/2010/06/literalreification/";

    public static final String FRBR_PREFIX = "frbr";
    public static final String FRBR_URI = "http://purl.org/vocab/frbr/core#";

    // http://www.w3.org/ns/prov#
    public static final String PROV_O_PREFIX = "provenance";
    public static final String PROV_O_URI = String.format(BASE_URI, PROV_O_PREFIX);

    public static final String SIOC_PREFIX = "sioc";
    public static final String SIOC = "http://rdfs.org/sioc/ns#";

    // Fields required in creating the RDF model
    public static final String PROVIDER_JDA = "JDA";
    public static final String PROVIDER_ZBW = "ZBW";
    public static final String PROVIDER_ZBW_LABEL_DE = "Leibniz-Informationszentrum Wirtschaft";
    public static final String PROVIDER_SCHOLEXPLORE = "ScholExplore";

    public static final String LANGUAGE_TAG_DE = "de";
    public static final String LANGUAGE_TAG_EN = "en";

    // Model values for classes and properties (All these values should be represented as constants!)
    // Classes
    public static final String A_CITATION = "Citation";
    public static final String EDM_AGENT = "Agent";
    public static final String EDM_PROVIDER = "provider";

    // Provenance-related classes and property string values
    public static final String PROV_GRAPH_NAME = "provenance";
    public static final String PROV_COLLECTION_CLASS = "Collection";
    public static final String PROV_GENERATION_CLASS = "Generation";
    public static final String ORIGINAL_COLLECTION_TITLE = "OpenAIRE ScholeXplorer Service: Scholix JSON Dump";
    public static final String ORIGINAL_COLLECTION_DESCRIPTION =
                "This datasets contains the GZ-compressed dump of the information space of the OpenAIRE ScholeXplorer" +
                " service. The dataset consists of 126+ Million literature-dataset and dataset-dataset links  between" +
                " 12+ Million objects, where links are encoded as records in Scholix format (schema Version 3). Links " +
                " were collected from publishers (CrossRef, EventData), data centers (DataCite), and institutional / " +
                "thematic repositories (OpenAIRE). The links are organized in 29 compressed files, each of ~500MB, for " +
                "a total of ~15GB.";
    public static final String PROV_GENERATION_COMMENT_VALUE = "RDF representation of the ScholExplorer link collection.";
    public static final String ORIGINAL_COLLECTION_FORMAT = "application/x-tar";
    public static final String PROV_HAD_MEMBER_PROPERTY = "hadMember";
    public static final String ORIGINAL_COLLECTION_INSTANCE = "scholexplorer-link-collection";
    public static final String RDF_COLLECTION_INSTANCE = "scholexplorer-link-collection-rdf";
    public static final String LINK_CONVERSION_VALUE = "link-collection-conversion";
    public static final String PROV_GENERATION_INSTANCE_VALUE = "prov-generation";
    public static final String ORIGNAL_COLLECTOIN_URI = "https://zenodo.org/record/2674330#.Xnoxq6hKhPZ";
    public static final String ORIGINAL_COLLECTION_LOCAL_PATH = "C:/Users/limani fidan/Downloads/scholix-data-dump/";

    // This is used to define the path of all constituting ScholExplorer files
    public static final String ORIGINAL_COLLECTION_FILE = "file:%s";
    public static final String PROV_AT_TIME_PROPERTY = "atTime";
    public static final String PROV_ACTIVITY_PROPERTY = "activity";
    public static final String PROV_ACTIVITY_CLASS = "Activity";
    public static final String PROV_SOFTWARE_AGENT_CLASS = "SoftwareAgent";
    public static final String PROV_ACTIVITY_INSTANCE = "conversion-workflow";
    public static final String PROV_ACTIVITY_START_TIME_VALUE = "startedAtTime";
    public static final String PROV_ACTIVITY_END_TIME_VALUE = "endedAtTime";
    public static final String PROV_USED_VALUE = "used";
    public static final String PROV_GENERATED_VALUE = "generated";
    public static final String PROV_WAS_GENERATED_BY_VALUE = "wasGeneratedBy";
    public static final String PROV_QUALIFIED_GENERATION_VALUE = "qualifiedGeneration";

    // Properties required for the relationship characteristics
    public static final String HAS_CITING_ENTITY = "hasCitingEntity";
    public static final String HAS_CITATION_CHARACTERIZATION = "hasCitationCharacterization";
    public static final String HAS_CITATION_CREATION_DATE = "hasCitationCreationDate";
    public static final String IS_CITED_AS_DATA_SOURCE_BY = "isCitedAsDataSourceBy";
    public static final String CITES_AS_DATA_SOURCE = "citesAsDataSource";
    public static final String CITES_AS_RELATED = "citesAsRelated";
    public static final String HAS_CITED_ENTITY = "hasCitedEntity";
    public static final String USES_IDENTIFIER_SCHEME = "usesIdentifierScheme";

    // Properties used to encode the Link relation type
    public static final String CITES_FOR_INFORMATION = "citesForInformation";
    public static final String IS_CITED_FOR_INFORMATION_BY = "isCitedForInformationBy";
    public static final String SUPPLEMENT = "supplement";
    public static final String SUPPLEMENT_OF = "supplementOf";
    public static final String IS_RELATED_TO = "isRelatedTo";

    // Property values: Miscellany
    public static final String PROPERTY_HAS_LITERAL_VALUE = "hasLiteralValue";
    public static final String PROPERTY_HAS_IDENTIFIER = "hasIdentifier";

    // Predefined values
    public static final String DOI_SCHEME = "doi";
    public static final String PUBLICATION_LABEL = "Publication";
    public static final String DATASET_LABEL = "Dataset";
    public static final String UNKNOWN_LABEL = "Unknown";
    public static final String PRIMARY_RESOURCE_ID = "PrimaryResourceIdentifier";
    public static final String ALTERNATE_RESOURCE_ID = "AlternateResourceIdentifier";
}