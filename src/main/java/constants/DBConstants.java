package constants;

/**
 * This static class contains constants that are used to access JDA's RESTful service.
 * @author Fidan Limani
 */

public class DBConstants {
    // This value is used to set the Jena TDB dataset to contain the ScholExplorer link collection
    public static final String SCHOL_LINK_DATASET = "dataset";
    public static final String SCHOL_EXPLORE_FUSEKI_URI = "http://localhost:3030/scholExplorer/data/%s";
}