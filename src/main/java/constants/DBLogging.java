package constants;

/**
 * This class contains the database-related logging constants for our application
 * @author Fidan Limani
 */
public class DBLogging {
    public static final String HARVEST_SOURCE = "Inserting in table <%s> ...";
    public static final String INSERTION_STATUS = "Inserting in table <%s> complete.";
}