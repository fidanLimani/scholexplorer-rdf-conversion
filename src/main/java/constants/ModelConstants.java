package constants;

/**
 * This static class contains constants that are used to construct JDA and EconBiz models, and ScholExplore links.
 * @author Fidan Limani
 */
public class ModelConstants
{
    // License specification for JDA metadata
    public static final String LINK_LICENSE = "http://creativecommons.org/publicdomain/zero/1.0/";
}